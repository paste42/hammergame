﻿using CodeNameSmash.Config;
using CodeNameSmash.Objects.Environment;
using CodeNameSmash.Objects.Player;
using CodeNameSmash.Scenes;
using Microsoft.Xna.Framework;
using PGOCS;
using PGOCS.Collisions;
using PGOCS.Components;
using PGOCS.Scenes;

namespace CodeNameSmash.Objects.Items
{
    class DroppedHammer: GameObject
    {
        public DroppedHammer(IGame game, Scene scene, Vector2 position) : base("droppedHammer", game, scene, position)
        {
            var sprite = new Sprite(game, "images/temphammer", new Point(32, 34), new Point(10, 1));
            sprite.SetFrame(7);
            AddComponent(sprite);
            var collider = new Collider(new Vector2(18, 13), new Vector2(0, 21), isSolid: false);
            collider.CollisionReaction = GetHammerBack;
            AddComponent(collider);
            var pb = new PhysicalBody(new Vector2(1f, 500f));
            pb.SetAccelerationY(HammerGameConfigManager.Instance.SceneGravity);
            AddComponent(pb);
        }

        public bool GetHammerBack(Collider other, Collision collision)
        {
            if (other.GameObject is PlayerCharacter)
            {
                (other.GameObject as PlayerCharacter).PickUpHammer();
                ((other.GameObject as PlayerCharacter).Scene as LevelScene).MarkGameObjectForRemoval(this);
                return false;
            }
            else if (other.GameObject is AltSwitch)
            {
                CollisionSystem.SeparateVerticallyFromTile(this, (Scene as TiledScene).Tilemap.GetTileCoords(other.GetAbsoluteTransform().Position),
                    (Scene as TiledScene).Tilemap.TileHeight, true);
                return true;
            }
            return true;
        }
    }
}
