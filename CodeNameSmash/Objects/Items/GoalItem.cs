﻿using CodeNameSmash.Objects.Player;
using CodeNameSmash.Scenes;
using Microsoft.Xna.Framework;
using PGOCS;
using PGOCS.Collisions;
using PGOCS.Components;
using PGOCS.Scenes;

namespace CodeNameSmash.Objects.Items
{
	class GoalItem : GameObject
	{
		readonly IGame game;
		public GoalItem(string id, IGame game, Scene scene, Point tilePosition, string texture) 
			: base(id, game, scene)
		{
			this.game = game;
			Transform.Position = new Vector2(tilePosition.X * ((TiledScene)Scene).Tilemap.TileWidth,
				tilePosition.Y * ((TiledScene)Scene).Tilemap.TileHeight);
			var sprite = new Sprite(game, texture,new Point(16, 16),new Point(1, 1));
			AddComponent(sprite);
			var collider = new Collider(new Vector2(16, 16), isMovable: false, isSolid: false);
			collider.CollisionReaction = EndLevel;
			AddComponent(collider);
		}

		bool EndLevel(Collider other, Collision collision)
		{
            // TODO: Change this so it goes to the right place
			if (other.GameObject.GetType() == typeof(PlayerCharacter))
				game.SwitchScene(new LevelScene(game, "basic2"));
            return false;
		}
	}
}
