﻿using CodeNameSmash.Config;
using CodeNameSmash.Objects.Environment;
using CodeNameSmash.Objects.Hammers;
using CodeNameSmash.Objects.Player;
using CodeNameSmash.Scenes;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PGOCS;
using PGOCS.Collisions;
using PGOCS.Components;
using PGOCS.Scenes;
using System;

namespace CodeNameSmash.Objects.Enemies
{
    class WalkingHat : Enemy
    {
        private float _direction = -1;
        Sprite _legsSprite;
        private bool _isDying = false;
        private bool _isCrunched = false;
        private bool _wasCrunchedThisFrame = false;

        public WalkingHat(string id, IGame game, Scene scene, Vector2 position) 
            : base(id, game, scene, position, new Sprite(game, "images/walkinghathat", new Point(16, 11), new Point(2, 1)), 
                new Collider(new Vector2(16, 16)))
        {
            var body = GetComponent<PhysicalBody>();
            if (body != null)
            {
                body.MaxVelocity = new Vector2(.1f, 500f);
                body.SetAccelerationY(HammerGameConfigManager.Instance.SceneGravity);
                body.SetVelocityX(-.1f);
            }
            var collider = GetComponent<Collider>();
            collider.CollisionReaction = ReactToCollisions;
            collider.IsMovingPlatform = true;
            _legsSprite = new Sprite(game, "images/walkinghatlegs", new Point(16, 6), new Point(8, 1), new Transform(new Vector2(0, 10)));
            _legsSprite.AddAnimation("walking", new int[8] { 0, 1, 2, 3, 4, 5, 6, 7 });
            _legsSprite.PlayAnimation("walking");
        }

        public override void Update(GameTime gameTime)
        {
            _wasCrunchedThisFrame = false;
            if (_isDying)
            {
                ((LevelScene)Scene).RemoveObject(this);
                return;
            }

            var body = GetComponent<PhysicalBody>();
            var collider = GetComponent<Collider>();
            if (body != null)
            {
                body.SetMaxVelocityX(HammerGameConfigManager.Instance.GoombaMaxSpeed);
                if (collider != null)
                {
                    if ((collider.TileCollisionFlag & CollisionSystem.CollisionFlags.Left) == CollisionSystem.CollisionFlags.Left)
                        _direction = 1;
                    if ((collider.TileCollisionFlag & CollisionSystem.CollisionFlags.Right) == CollisionSystem.CollisionFlags.Right)
                        _direction = -1;
                }

                body.SetVelocityX(_direction * body.MaxVelocity.X);
            }
            base.Update(gameTime);
            _legsSprite.Update(gameTime);
        }

        public static void Spawn(string id, IGame game, Scene scene, Vector2 position)
        {
            WalkingHat hat = new WalkingHat(id, game, scene, position);
            ((LevelScene)scene).AddEnemy(hat);
            scene.Add(hat);
        }

        public override void Draw(SpriteBatch sb, Camera camera)
        {
            var hatSprite = GetComponent<Sprite>();
            if (_direction > 0)
            {
                hatSprite.IsFlippedHorizontally = true;
                _legsSprite.IsFlippedHorizontally = true;
            }
            else
            {
                hatSprite.IsFlippedHorizontally = false;
                _legsSprite.IsFlippedHorizontally = false;
            }
            _legsSprite.Draw(sb, Transform, camera);
            hatSprite.Draw(sb, Transform, camera);
            DrawColliderOverlay(sb, camera, hatSprite);
        }

        public void Crunch()
        {
            if (_isCrunched) return;
            _isCrunched = true;
            _wasCrunchedThisFrame = true;
            GetComponent<Sprite>().CurrentFrame = 1;
            var collider = GetComponent<Collider>();
            collider.Position = new Vector2(0, 8);
            collider.Size = new Vector2(16, 8);
        }

        public bool ReactToCollisions(Collider other, Collision collision)
        {
            // TODO: change this hammer reaction so it's in LandedState and SideSwungState
            if (other.GameObject is Hammer)
            {
                if (!_isCrunched) Crunch();
                else if (!_wasCrunchedThisFrame)_isDying = true;
                return false;
            }
            else if ((other.GameObject is Nail) && ((other.GameObject as Nail).IsSinking))
            {
                _isDying = true;
                return false;
            }
            else if (other.GameObject is PressurePlate)
            {
                return false;
            }
            if (!(other.GameObject is PlayerCharacter))
            {
                if (collision.PrimaryFlags.HasFlag(CollisionSystem.CollisionFlags.Left) || collision.PrimaryFlags.HasFlag(CollisionSystem.CollisionFlags.Right))
                    _direction = -_direction;
            }
            else
            {
                var flag = (other == collision.PrimaryCollider) ? collision.PrimaryFlags : collision.SecondaryFlags;
                if (flag.HasFlag(CollisionSystem.CollisionFlags.Left)) _direction = -Math.Abs(_direction);
                else if (flag.HasFlag(CollisionSystem.CollisionFlags.Right)) _direction = Math.Abs(_direction);
            }
            return true;
        }
    }
}
