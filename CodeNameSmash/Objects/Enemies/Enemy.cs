﻿using Microsoft.Xna.Framework;
using PGOCS;
using PGOCS.Components;
using PGOCS.Scenes;

namespace CodeNameSmash.Objects.Enemies
{
    public abstract class Enemy : GameObject
    {
        protected Collider Collider;
        protected Sprite Sprite;
        protected Enemy(string id, IGame game, Scene scene, Vector2 position, Sprite sprite, Collider collider, float depth = 0f) 
            : base(id, game, scene, position) 
        {
            Collider = collider;
            AddComponent(collider);
            Sprite = sprite;
	        Sprite.Transform.Depth = .2f;
            AddComponent(sprite);
            AddComponent(new PhysicalBody());
        }
    }
}
