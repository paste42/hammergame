﻿using CodeNameSmash.Config;
using CodeNameSmash.Objects.Environment;
using CodeNameSmash.Objects.Hammers;
using CodeNameSmash.Objects.Player;
using CodeNameSmash.Scenes;
using Microsoft.Xna.Framework;
using PGOCS;
using PGOCS.Collisions;
using PGOCS.Components;
using PGOCS.Scenes;
using System;

namespace CodeNameSmash.Objects.Enemies
{
    public class Goomba : Enemy
    {
        // TODO: Make it so goombas get squished when rolled over
	    private float _direction = -1;
	    private bool _isDying = false;

        public Goomba(string id, IGame game, Scene scene, Vector2 position, float depth = 0f)
            : base(id, game, scene, position, new Sprite(game, "images/goomba", new Point(14, 16), new Point(2,1)),
			new Collider(new Vector2(14, 16)), depth)
        {
			var body = GetComponent<PhysicalBody>();
	        if (body != null)
	        {
		        body.MaxVelocity = new Vector2(.1f, 500f);
		        body.SetAccelerationY(HammerGameConfigManager.Instance.SceneGravity);
		        body.SetVelocityX(-.1f);
	        }
	        Sprite.AddAnimation("walk", new []{0, 1}, 200);
            GetComponent<Collider>().CollisionReaction = ReactToCollisions;
        }

		public override void Update(GameTime gameTime)
		{
			if (_isDying)
			{
				((LevelScene)Scene).RemoveObject(this);
				return;
			}
			var body = GetComponent<PhysicalBody>();
			var collider = GetComponent<Collider>();

			if (body != null)
			{
				body.SetMaxVelocityX(HammerGameConfigManager.Instance.GoombaMaxSpeed);
				if (collider != null)
				{
					if ((collider.TileCollisionFlag & CollisionSystem.CollisionFlags.Left) == CollisionSystem.CollisionFlags.Left)
						_direction = 1;
					if ((collider.TileCollisionFlag & CollisionSystem.CollisionFlags.Right) == CollisionSystem.CollisionFlags.Right)
						_direction = -1;
				}

				body.SetVelocityX(_direction * body.MaxVelocity.X);
			}
			Sprite.PlayAnimation("walk");

			base.Update(gameTime);
		}

        public bool ReactToCollisions(Collider other, Collision collision)
        {
            // TODO: change this hammer reaction so it's in LandedState and SideSwungState
            if (other.GameObject is Hammer)
            {
                _isDying = true;
                return false;
            }
            else if ((other.GameObject is Nail) &&  ((other.GameObject as Nail).IsSinking))
            {
                _isDying = true;
                return false;
            }
            else if (other.GameObject is PressurePlate)
            {
                return false;
            }
            if (!(other.GameObject is PlayerCharacter))
            {
                if (collision.PrimaryFlags.HasFlag(CollisionSystem.CollisionFlags.Left) || collision.PrimaryFlags.HasFlag(CollisionSystem.CollisionFlags.Right))
                    _direction = -_direction;
            }
            else
            {
                var flag = (other == collision.PrimaryCollider) ? collision.PrimaryFlags : collision.SecondaryFlags;
                if (flag.HasFlag(CollisionSystem.CollisionFlags.Left)) _direction = -Math.Abs(_direction);
                else _direction = Math.Abs(_direction);
            }
            return true;
        }

        public static void Spawn(string id, IGame game, Scene scene, Vector2 position, float depth = 0f)
	    {
			Goomba goomba = new Goomba(id, game, scene, position, depth);
		    ((LevelScene)scene).AddEnemy(goomba);
			scene.Add(goomba);
	    }

        public void Die()
        {
            _isDying = true;
        }
    }
}
