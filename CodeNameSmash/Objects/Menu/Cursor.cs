﻿using CodeNameSmash.Objects.Interfaces;
using Microsoft.Xna.Framework;
using PGOCS;
using PGOCS.Components;
using PGOCS.Scenes;

namespace CodeNameSmash.Objects.Menu
{
	class Cursor : GameObject, IVerticalMover, IHorizontalMover
	{
		private bool _moveUpFlag;
		private bool _moveDownFlag;
		private bool _moveLeftFlag;
		private bool _moveRightFlag;

		public Cursor(string id, IGame game, Scene scene, Vector2 position = default(Vector2))
			: base(id, game, scene, position)
		{
			var sprite = new Sprite(game, "images/cursor", new Point(20, 20), new Point(1, 1));
			AddComponent(sprite);
		}

		public override void Update(GameTime gameTime)
		{
			if (_moveUpFlag && !_moveDownFlag) Transform.Position = new Vector2(Transform.Position.X, Transform.Position.Y - 32);
			if (!_moveUpFlag && _moveDownFlag) Transform.Position = new Vector2(Transform.Position.X, Transform.Position.Y + 32);
			if (_moveLeftFlag && !_moveRightFlag) Transform.Position = new Vector2(Transform.Position.X - 32, Transform.Position.Y);
			if (!_moveLeftFlag && _moveRightFlag) Transform.Position = new Vector2(Transform.Position.X + 32, Transform.Position.Y);
			ResetInputTriggers();
			base.Update(gameTime);
		}

		private void ResetInputTriggers()
		{
			_moveDownFlag = false;
			_moveLeftFlag = false;
			_moveRightFlag = false;
			_moveUpFlag = false;
		}

		public void MoveUp()
		{
			_moveUpFlag = true;
		}

		public void MoveDown()
		{
			_moveDownFlag = true;
		}

		public void MoveRight()
		{
			_moveRightFlag = true;
		}

		public void MoveLeft()
		{
			_moveLeftFlag = true;
		}
	}
}
