﻿using Microsoft.Xna.Framework;
using PGOCS;
using PGOCS.Components;
using PGOCS.Scenes;

namespace CodeNameSmash.Objects.Menu
{
	class LevelIcon : GameObject
	{
		private Sprite _sprite;
		public LevelIcon(string id, IGame game, Scene scene, Vector2 position = new Vector2()) 
			: base(id, game, scene, position)
		{
			_sprite = new Sprite(game, "images/tempmenulevelicon", new Point(16, 16), new Point(1, 1));
			AddComponent(_sprite);
		}
	}
}
