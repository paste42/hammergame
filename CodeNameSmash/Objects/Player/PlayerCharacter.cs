﻿using System;
using System.Linq;
using CodeNameSmash.Config;
using CodeNameSmash.Objects.Enemies;
using CodeNameSmash.Objects.Environment;
using CodeNameSmash.Objects.Hammers;
using CodeNameSmash.Objects.Hammers.States;
using CodeNameSmash.Objects.Interfaces;
using CodeNameSmash.Objects.Player.States;
using CodeNameSmash.Scenes;
using Microsoft.Xna.Framework;
using PGOCS;
using PGOCS.Collisions;
using PGOCS.Components;
using PGOCS.Scenes;

namespace CodeNameSmash.Objects.Player
{
    class PlayerCharacter : GameObject, IJumper, IHorizontalMover, IAttacker
    {
        private readonly Sprite _sprite;
	    public bool IsOnGround { get; private set; }
	    public bool IsJumping { get; private set; }
        public bool IsAttacking { get; private set; }
        public bool IsEnteringDoor { get; private set; }
	    public bool IsFacingRight
	    {
			get { return !_sprite.IsFlippedHorizontally; }
		    private set
		    {
			    _sprite.IsFlippedHorizontally = !value;
			    Hammer.IsFacingRight = value;
		    }
		}

        public bool HasSpringBoots;
		public bool JumpLock { get; set; }
	    private bool _moveRightTrigger;
	    private bool _moveLeftTrigger;
	    private bool _attackTrigger;
		private float _secondaryJumpSpeed;
	    private PlayerState _currentState;
        public Hammer Hammer { get; private set; }

        public PlayerCharacter(string id, IGame game, Scene scene, string spriteSheet, Vector2 position = new Vector2()) 
			: base (id, game, scene, position)
        {
            
            _sprite = new Sprite(game, spriteSheet, new Point(16, 16), new Point(4, 1), 
				new Transform(new Vector2(-8f, -16f)));
			Transform.GameObject = this;
			Transform.Depth = .1f; 
			AddComponent(_sprite);
	        var collider = new Collider(new Vector2(10, 15), new Vector2(-5, -15), horizontalTolerance: 2.2f)
	        {
		        TileCollisionReaction = HitHead,
                CollisionReaction = ReactToCollisions,
		        UseDefaultAndCustomReaction = true
	        };
	        AddComponent(collider);
            AddComponent(new PhysicalBody(new Vector2(1f, 500)));
	        var physicalBody = GetComponent<PhysicalBody>();
	        if (physicalBody != null)
		        physicalBody.SetAccelerationY(HammerGameConfigManager.Instance.SceneGravity);
	        _sprite.AddAnimation("run", new[]{0, 1, 2, 3});
	        Hammer = new Hammer(game, scene, this) {Transform = {Depth = -.01f}};
	        AddChild(Hammer);
	        ResetInputTriggers();
			_currentState = new WalkingState(this);
            IsEnteringDoor = false;
            // TODO: Change this to get data from save file
            HasSpringBoots = false;
        }


        public override void Update(GameTime gameTime)
        {
	        if (IsPaused) return;
			var collider = GetComponent<Collider>();
			if (collider != null) IsOnGround = collider.IsOnGround;
	        var body = GetComponent<PhysicalBody>();
	        body.SetMaxVelocityX(HammerGameConfigManager.Instance.PlayerMaxSpeed);
		    body.SetAccelerationY(HammerGameConfigManager.Instance.SceneGravity);
		    _secondaryJumpSpeed = HammerGameConfigManager.Instance.PlayerSecondaryJumpSpeed;
			if (IsOnGround) body.SetVelocityY(0f);
			if (!IsAttacking)
			{
				HorizontalMovement(body);
				HandleJumping(body);
			}
            base.Update(gameTime);
			HandleAttacking(body);
			ResetInputTriggers();
        }

	    public void ChangeStateTo(PlayerState state)
	    {
			_currentState.Exit();
		    _currentState = state;
			_currentState.Enter();
	    }

	    private void ResetInputTriggers()
	    {
			IsJumping = false;
		    _attackTrigger = false;
		    _moveRightTrigger = false;
			_moveLeftTrigger = false;
		}

	    public override void LateUpdate(GameTime gameTime)
	    {
		    if (IsPaused) return;
		    if (Hammer.IsSlamming &&
		        Math.Abs(GetComponent<PhysicalBody>().Velocity.Y) < .001)
			    Hammer.IsSlamming = false;
            var collider = GetComponent<Collider>();
            if (Hammer.CurrentHammerState is PokingState)
            {
                if (IsFacingRight && ((collider.CollisionFlag & CollisionSystem.CollisionFlags.Left) != CollisionSystem.CollisionFlags.None))
                {
                    foreach (var barrel in GetComponent<Collider>().CollisionList.Where(x => x is Barrel).Select(x => x as Barrel))
                        if ((barrel.Transform.Position.X + barrel.GetComponent<Collider>().Size.X == Transform.Position.X + GetComponent<Collider>().Position.X)
                            && ((barrel.GetComponent<Collider>().CollisionFlag & (CollisionSystem.CollisionFlags.Left | CollisionSystem.CollisionFlags.Right)) == (CollisionSystem.CollisionFlags.Left | CollisionSystem.CollisionFlags.Right)))
                        (Hammer.CurrentHammerState as PokingState).Retract(gameTime);
                }
                else if (!IsFacingRight && ((collider.CollisionFlag & CollisionSystem.CollisionFlags.Right) != CollisionSystem.CollisionFlags.None))
                {
                    foreach (var barrel in GetComponent<Collider>().CollisionList.Where(x => x is Barrel).Select(x => x as Barrel))
                        if ((barrel.Transform.Position.X == Transform.Position.X + GetComponent<Collider>().Size.X + GetComponent<Collider>().Position.X)
                            && ((barrel.GetComponent<Collider>().CollisionFlag & (CollisionSystem.CollisionFlags.Left | CollisionSystem.CollisionFlags.Right)) == (CollisionSystem.CollisionFlags.Left | CollisionSystem.CollisionFlags.Right)))
                        (Hammer.CurrentHammerState as PokingState).Retract(gameTime);
                }
                if ((IsFacingRight && ((collider.TileCollisionFlag & CollisionSystem.CollisionFlags.Left) != CollisionSystem.CollisionFlags.None))
                    || (!IsFacingRight && ((collider.TileCollisionFlag & CollisionSystem.CollisionFlags.Right) != CollisionSystem.CollisionFlags.None)))
                    (Hammer.CurrentHammerState as PokingState).Retract(gameTime);
            }
                base.LateUpdate(gameTime);
            IsEnteringDoor = false;
	    }

	    private void HorizontalMovement(PhysicalBody body)
	    {
			if (_moveRightTrigger)
			{
				body.SetAccelerationX(HammerGameConfigManager.Instance.PlayerAccelerationMagnitude);
				if (!IsOnGround) return;
				IsFacingRight = true;
				if (_currentState is WalkingState) Run();
			}
			else if (_moveLeftTrigger)
			{
				body.SetAccelerationX(-HammerGameConfigManager.Instance.PlayerAccelerationMagnitude);
				if (!IsOnGround) return;
				IsFacingRight = false;
				if (_currentState is WalkingState) Run();
			}
			else
			{
                if (Math.Abs(body.Velocity.X) < .0001f)
                {
                    StopRunning();
                    body.SetAccelerationX(0);
                    body.SetVelocityX(0);
                }
                else ApplyAirResistance(body);
            }
	    }

	    protected void ApplyAirResistance(PhysicalBody body)
	    {
			float newVelocity = Math.Abs(body.Velocity.X) - HammerGameConfigManager.Instance.PlayerAccelerationMagnitude;
			body.SetVelocityX(Math.Sign(body.Velocity.X) * Math.Max(newVelocity, 0));
            body.SetAccelerationX(0f);
	    }

	    private void HandleJumping(PhysicalBody body)
	    {
		    if (JumpLock && IsJumping) return;
		    JumpLock = false;
			if (IsJumping)
			{
				if (!(_currentState is JumpingState))
				{
					ChangeStateTo(new JumpingState(this));
                    var collider = GetComponent<Collider>();
                    if (HasSpringBoots) body.SetVelocityY(HammerGameConfigManager.Instance.PlayerSpringBootsJumpSpeed);
                    else body.SetVelocityY(HammerGameConfigManager.Instance.PlayerInitialJumpSpeed);
                    if ((collider.IsOnMovingPlatform != null) && (collider.IsOnMovingPlatform.GameObject is SlideWall)
                        && ((collider.IsOnMovingPlatform.GameObject as SlideWall).SlideStatus == SlideWall.SlideStatuses.MovingUp))
                        body.Velocity += new Vector2(0, -90);
                    collider.IsOnGround = false;
                    collider.IsOnMovingPlatform = null;
                }
                StopRunning();
				if (body.Velocity.Y >= 0) ChangeStateTo(new FallingState(this));
			}
			else if (!IsOnGround) ChangeStateTo(new FallingState(this));
			else if (_currentState is FallingState && body.Velocity.Y < _secondaryJumpSpeed)
				body.SetVelocityY(_secondaryJumpSpeed);
			else ChangeStateTo(new WalkingState(this));
			if (!IsOnGround) _sprite.SetFrame(1);
	    }

	    private void HandleAttacking(PhysicalBody body)
	    {
			if (_attackTrigger && Hammer.CanSwing()) InitiateAttack();
			else if (Hammer.IsSwinging) ContinueAttack(body);
			else if (IsAttacking)
			{
				IsAttacking = false;
				Hammer.StopSwinging();
			}
	    }

	    private void InitiateAttack()
	    {
			Hammer.Swing();
		    if (IsOnGround) ChangeStateTo(new AttackingState(this));
		    else ChangeStateTo(new AirAttackingState(this));
			IsAttacking = true;
			Hammer.IsAirAttacking = !IsOnGround;
			StopRunning();
	    }

	    private void ContinueAttack(PhysicalBody body)
	    {
			body.SetVelocityX(0);
			body.SetAccelerationX(0);
			if (Hammer.IsBlocked)
			{
				if (body.Velocity.Y <= 0) return;
				body.SetAccelerationY(0);
				body.SetVelocityY(0);
				return;
			}
			StopRunning();
			if (!(_currentState is AirAttackingState)) return;
			body.SetAccelerationY(0);
		    body.SetVelocityY(Hammer.IsSlamming ? 120 : 0);
			if (IsOnGround)
			{
				Hammer.IsAirAttacking = false;
				ChangeStateTo(new FallingState(this));
			}
	    }

        private void Run()
        {
            _sprite.PlayAnimation("run");
        }

        private void StopRunning()
        {
            _sprite.StopAnimation();
            _sprite.SetFrame(0);
        }

	    public void Jump()
	    {
		    if (!(_currentState is IJumper)) return;
			((IJumper)_currentState).Jump();
		    IsJumping = true;
	    }

	    public void MoveRight()
	    {
			if (!(_currentState is IHorizontalMover)) return;
			((IHorizontalMover)_currentState).MoveRight();
		    _moveRightTrigger = true;
	    }

	    public void MoveLeft()
	    {
			if (!(_currentState is IHorizontalMover)) return;
			((IHorizontalMover)_currentState).MoveLeft();
			_moveLeftTrigger = true;
	    }

	    public void Attack()
	    {
		    if (!(_currentState is IAttacker)) return;
			((IAttacker)_currentState).Attack();
		    _attackTrigger = true;
	    }

        public void EnterDoor()
        {
            IsEnteringDoor = true;
        }

	    public void SetSideSwingModifier()
	    {
		    Hammer.CurrentSwingType = Hammer.SwingType.Side;
	    }

        public void SetDropModifier()
        {
            if (!IsOnGround)
                Hammer.CurrentSwingType = Hammer.SwingType.Drop;
        }

	    public void HitHead()
	    {
			if ((GetComponent<Collider>().TileCollisionFlag &
				CollisionSystem.CollisionFlags.Top) == CollisionSystem.CollisionFlags.Top)
				GetComponent<PhysicalBody>().SetVelocityY(0f);
        }

        public bool ReactToCollisions(Collider other, Collision collision)
        {
            var colliderFlags = GetComponent<Collider>().CollisionFlag;
            if ((_currentState is AttackingState || _currentState is AirAttackingState) && Hammer.CurrentHammerState is PokingState
                && ((IsFacingRight && ((colliderFlags & CollisionSystem.CollisionFlags.Left) != CollisionSystem.CollisionFlags.None))
                || (!IsFacingRight && ((colliderFlags & CollisionSystem.CollisionFlags.Right) != CollisionSystem.CollisionFlags.None))))
            {
                if (other.GameObject is Barrel)
                {
                    (other.GameObject as Barrel).Slide(!IsFacingRight);
                    return true;
                }
            }
            if ((other.GameObject is Goomba) && (collision.PrimaryFlags.HasFlag(CollisionSystem.CollisionFlags.Bottom)))
            {
                (other.GameObject as Goomba).Die();
                var body = GetComponent<PhysicalBody>();
                if (body.Velocity.Y > 0) body.Velocity = new Vector2(body.Velocity.X, -body.Velocity.Y - HammerGameConfigManager.Instance.SceneGravity);
                return false;
            }
            if (other.GameObject is PressurePlate) 
                (other.GameObject as PressurePlate).Activate();
            return true;
        }

        public void PickUpHammer()
        {
            Hammer.PickUp();
        }

        public void SetPokeModifier()
        {
            Hammer.CurrentSwingType = Hammer.SwingType.Poke;
        }
    }
}
