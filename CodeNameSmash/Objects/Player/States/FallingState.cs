﻿using CodeNameSmash.Objects.Interfaces;

namespace CodeNameSmash.Objects.Player.States
{
	class FallingState : PlayerState, IHorizontalMover, IAttacker
	{
		public FallingState(PlayerCharacter player) : base(player) { }

		public void MoveRight() { }

		public void MoveLeft() { }

		public void Attack() { }

		public void SetSideSwingModifier() { }

        public void SetPokeModifier() { }

        public void SetDropModifier() { }

		public override void Exit()
		{
			Player.JumpLock = true;
		}
	}
}
