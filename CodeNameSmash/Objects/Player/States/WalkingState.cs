﻿using CodeNameSmash.Objects.Interfaces;

namespace CodeNameSmash.Objects.Player.States
{
	class WalkingState : PlayerState, IJumper, IHorizontalMover, IAttacker
	{
		public WalkingState(PlayerCharacter player) : base(player) { }

		public void Jump() { }

		public void MoveRight() { }

		public void MoveLeft() { }

		public void Attack() { }
		public void SetSideSwingModifier() { }
        public void SetPokeModifier() { }
        public void SetDropModifier() { }
    }
}
