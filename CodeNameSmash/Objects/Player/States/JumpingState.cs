﻿using CodeNameSmash.Objects.Interfaces;

namespace CodeNameSmash.Objects.Player.States
{
	class JumpingState : PlayerState, IJumper, IAttacker, IHorizontalMover
	{
		public JumpingState(PlayerCharacter player) : base(player) { }

		public void Jump() { }

		public void Attack() { }
		public void SetSideSwingModifier() { }
        public void SetPokeModifier() { }
        public void SetDropModifier() { }

        public void MoveRight() { }
		public void MoveLeft() { }
	}
}
