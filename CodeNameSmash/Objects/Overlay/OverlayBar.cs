﻿using Microsoft.Xna.Framework;
using PGOCS;
using PGOCS.Components;
using PGOCS.Scenes;

namespace CodeNameSmash.Objects.Overlay
{
	public class OverlayBar : GameObject
	{
		private Sprite _sprite;
		public OverlayBar(IGame game, Scene scene, string texture) : base("overlaybar", game, scene, new Vector2(0, 160))
		{
			_sprite = new Sprite(game, texture, new Point(320, 20), new Point(1,1));
			AddComponent(_sprite);
		}
	}
}
