﻿namespace CodeNameSmash.Objects.Interfaces
{
	public interface IJumper
	{
		void Jump();
	}
}
