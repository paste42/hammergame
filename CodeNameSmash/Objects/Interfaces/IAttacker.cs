﻿namespace CodeNameSmash.Objects.Interfaces
{
	public interface IAttacker
	{
		void Attack();
		void SetSideSwingModifier();
        void SetDropModifier();
        void SetPokeModifier();
	}
}
