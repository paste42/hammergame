﻿namespace CodeNameSmash.Objects.Interfaces
{
	public interface IVerticalMover
	{
		void MoveUp();
		void MoveDown();
	}
}
