﻿namespace CodeNameSmash.Objects.Interfaces
{
	public interface IHorizontalMover
	{
		void MoveRight();
		void MoveLeft();
	}
}
