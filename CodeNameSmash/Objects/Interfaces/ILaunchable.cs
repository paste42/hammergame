﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeNameSmash.Objects.Interfaces
{
    interface ILaunchable
    {
        void Launch();
    }
}
