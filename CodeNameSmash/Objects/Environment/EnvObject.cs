﻿using Microsoft.Xna.Framework;
using PGOCS;
using PGOCS.Components;
using PGOCS.Scenes;

namespace CodeNameSmash.Objects.Environment
{
	public class EnvObject : GameObject
	{
		protected Sprite Sprite;
		public EnvObject(string id, IGame game, Scene scene, Sprite sprite, Vector2 position = new Vector2())
			: base(id, game, scene, position)
		{
			Sprite = sprite;
			Sprite.Transform.Depth = .3f;
			AddComponent(Sprite);
		}
	}
}
