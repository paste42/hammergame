﻿using CodeNameSmash.Objects.Player;
using CodeNameSmash.Scenes;
using Microsoft.Xna.Framework;
using PGOCS;
using PGOCS.Collisions;
using PGOCS.Components;
using PGOCS.Scenes;
using System.Collections.Generic;

namespace CodeNameSmash.Objects.Environment
{
    class PressurePlate : EnvObject
    {
        private bool _collided;
        private bool _justReleased;
        private List<EnvObject> _associatedObjects;
        public PressurePlate(string id, IGame game, Scene scene, Point tilePosition) 
            : base(id, game, scene, new Sprite(game, "images/pressureplate", new Point(16, 3), new Point(1,1)))
        {
            Transform.Position = new Vector2(tilePosition.X * ((TiledScene)scene).Tilemap.TileWidth, tilePosition.Y * ((TiledScene)scene).Tilemap.TileHeight + 13);
            _associatedObjects = new List<EnvObject>();
            var collider = new Collider(new Vector2(12, 3), new Vector2(2, 0), false, false);
            collider.CollisionReaction = ActivateAssociatedObjects;
            collider.StillCollidingReaction = StayPressed;
            AddComponent(collider);
            _collided = false;
            _justReleased = false;
        }

        public override void Update(GameTime gameTime)
        {
            if (_collided)
            {
                _collided = false;
                _justReleased = true;
            }
            else if (_justReleased)
            {
                foreach (EnvObject spike in _associatedObjects)
                    ((PressureSpike)spike).Deactivate();
                _justReleased = false;
            }
            base.Update(gameTime);
        }

        public void AddAssociatedEnvObject(EnvObject envObject)
        {
            _associatedObjects.Add(envObject);
        }

        public bool ActivateAssociatedObjects(Collider other, Collision collision)
        {
            _collided = true;
            _justReleased = false;
            return false;
        }

        public void StayPressed(Collider other)
        {
            _collided = true;
            _justReleased = false;
        }

        public void Activate()
        {
            foreach (EnvObject spike in _associatedObjects)
                ((PressureSpike)spike).Activate();
        }
    }
}
