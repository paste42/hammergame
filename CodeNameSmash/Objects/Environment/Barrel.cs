﻿using CodeNameSmash.Config;
using CodeNameSmash.Objects.Enemies;
using CodeNameSmash.Objects.Hammers;
using CodeNameSmash.Objects.Hammers.States;
using CodeNameSmash.Objects.Interfaces;
using CodeNameSmash.Scenes;
using Microsoft.Xna.Framework;
using PGOCS;
using PGOCS.Collisions;
using PGOCS.Components;
using PGOCS.Scenes;

namespace CodeNameSmash.Objects.Environment
{
    public class Barrel : EnvObject, ILaunchable
    {
        public enum BarrelStates { Upright, OnSide, Rolling, Sliding }
        public BarrelStates CurrentState { get; private set; }
        public readonly int RollingSpeed = 1;

        private bool _movingRight;
        private int _movementCounter;
        private bool _isLaunched = false;

        public Barrel(string id, IGame game, Scene scene, Point tilePosition, bool isUpright)
            : base(id, game, scene, new Sprite(game, "images/barrel", new Point(16, 16), new Point(9, 1)))
        {
            Transform.Position = new Vector2(tilePosition.X * ((TiledScene)scene).Tilemap.TileWidth, tilePosition.Y * ((TiledScene)scene).Tilemap.TileHeight);
            var body = new PhysicalBody();
            body.MaxVelocity = new Vector2(.1f, 500f);
            body.SetAccelerationY(HammerGameConfigManager.Instance.SceneGravity);
            AddComponent(body);
            var collider = new Collider(new Vector2(16, 16));
            collider.CollisionReaction = ReactToCollisions;
            AddComponent(collider);
            if (isUpright)
            {
                CurrentState = BarrelStates.Upright;
                Sprite.CurrentFrame = 0;
            }
            else
            {
                CurrentState = BarrelStates.OnSide;
                Sprite.CurrentFrame = 1;
            }
            Sprite.AddAnimation("rollingright", new int[8] { 1, 2, 3, 4, 5, 6, 7, 8 });
            Sprite.AddAnimation("rollingleft", new int[8] { 1, 8, 7, 6, 5, 4, 3, 2 });
        }


        public void Roll(bool toTheRight)
        {
            if (CurrentState != BarrelStates.OnSide) return;
            _movingRight = toTheRight;
            var position = Transform.GetAbsoluteTransform().Position;
            if (_movingRight)
            {
                Sprite.PlayAnimation("rollingright", startingFrame: Sprite.CurrentFrame - 1);
                Transform.SetAbsolutePosition(new Vector2(position.X + RollingSpeed, position.Y));
            }
            else
            {
                Sprite.PlayAnimation("rollingleft", startingFrame: Sprite.CurrentFrame - 1);
                Transform.SetAbsolutePosition(new Vector2(position.X - RollingSpeed, position.Y));
            }
            CurrentState = BarrelStates.Rolling;
        }

        public void KnockOver()
        {
            if (CurrentState == BarrelStates.Upright) CurrentState = BarrelStates.OnSide;
            Sprite.SetFrame(1);
        }

        public void Slide(bool toTheRight)
        {
            if (CurrentState != BarrelStates.Upright) return;
            CurrentState = BarrelStates.Sliding;
            _movementCounter = 1;
            _movingRight = toTheRight;
            var position = Transform.GetAbsoluteTransform().Position;
            if (_movingRight) Transform.SetAbsolutePosition(new Vector2(position.X + 1, position.Y));
            else Transform.SetAbsolutePosition(new Vector2(position.X - 1, position.Y));
        }

        public void Launch()
        {
            _isLaunched = true;
        }

        public void StopSliding()
        {
            CurrentState = BarrelStates.Upright;
        }

        public void StopRolling()
        {
            CurrentState = BarrelStates.OnSide;
            Sprite.StopAnimation();
        }

        public override void Update(GameTime gameTime)
        {
            if (GetComponent<Collider>().IsOnGround) _isLaunched = false;
            switch (CurrentState)
            {
                case BarrelStates.Sliding:
                    var position = Transform.GetAbsoluteTransform().Position;
                    if (_movingRight)
                        Transform.SetAbsolutePosition(new Vector2(position.X + 1, position.Y));
                    else
                        Transform.SetAbsolutePosition(new Vector2(position.X - 1, position.Y));
                    _movementCounter++;
                    if (_movementCounter >= 16) StopSliding();
                    break;
                case BarrelStates.Rolling:
                    var position2 = Transform.GetAbsoluteTransform().Position;
                    var collider = GetComponent<Collider>();
                    if (_isLaunched)
                    {
                        if (_movingRight)
                            Transform.SetAbsolutePosition(new Vector2(position2.X + RollingSpeed, position2.Y));
                        else
                            Transform.SetAbsolutePosition(new Vector2(position2.X - RollingSpeed, position2.Y));
                    }
                    else if (collider.IsOnGround)
                    {
                        if (_movingRight)
                        {
                            if ((collider.TileCollisionFlag & CollisionSystem.CollisionFlags.Right) != CollisionSystem.CollisionFlags.None)
                                StopRolling();
                            else
                                Transform.SetAbsolutePosition(new Vector2(position2.X + RollingSpeed, position2.Y));
                        }
                        else
                        {
                            if ((collider.TileCollisionFlag & CollisionSystem.CollisionFlags.Left) != CollisionSystem.CollisionFlags.None)
                                StopRolling();
                            else
                                Transform.SetAbsolutePosition(new Vector2(position2.X - RollingSpeed, position2.Y));
                        }
                    }
                    break;
                default:
                    break;
            }

            base.Update(gameTime);
        }

        public override void LateUpdate(GameTime gameTime)
        {
            if ((CurrentState == BarrelStates.Sliding) && (Transform.GetAbsoluteTransform().Position == Transform.GetAbsolutePreviousPosition()))
                StopSliding();
            if ((CurrentState == BarrelStates.Rolling) && (Transform.GetAbsoluteTransform().Position == Transform.GetAbsolutePreviousPosition()))
                StopRolling();
            base.LateUpdate(gameTime);
        }

        public bool ReactToCollisions(Collider other, Collision collision)
        {
            if (other.GameObject is Hammer)
                return false;
            if (other.GameObject is PressurePlate)
            {
                (other.GameObject as PressurePlate).Activate();
                return false;
            }
            if (other.GameObject is Enemy)
                return false;
            var collider = GetComponent<Collider>();
            bool collided = collider.CheckSimpleCollision(other);
            if (!collided && ((collider.CollisionFlag & CollisionSystem.CollisionFlags.Bottom) != CollisionSystem.CollisionFlags.None)
                && ((other.CollisionFlag & CollisionSystem.CollisionFlags.Top) != CollisionSystem.CollisionFlags.None))
            {
                collider.IsOnGround = false;
                return false;
            }
            return true;
        }
    }
}
