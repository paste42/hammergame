﻿using CodeNameSmash.Scenes;
using Microsoft.Xna.Framework;
using PGOCS;
using PGOCS.Collisions;
using PGOCS.Components;
using PGOCS.Scenes;

namespace CodeNameSmash.Objects.Environment
{
    class PressureSpike : EnvObject
    {
        public PressureSpike(string id, IGame game, Scene scene, Point tilePosition) 
            : base(id, game, scene, new Sprite(game, "images/spikes", new Point(16, 16), new Point(1, 1)))
        {
            Transform.Position = new Vector2(tilePosition.X * ((TiledScene)scene).Tilemap.TileWidth, tilePosition.Y * ((TiledScene)scene).Tilemap.TileHeight);
            var collider = new Collider(new Vector2(16, 16), isMovable: false);
            collider.CollisionReaction = KillPlayer;
            AddComponent(collider);
            Deactivate();

        }

        public bool KillPlayer(Collider other, Collision collision)
        {
            if (other.GameObject is Player.PlayerCharacter)
            {
                // TODO: change this to use Collision in case of possible incorrect flags
                var colliderFlags = GetComponent<Collider>().CollisionFlag;
                if ((((other.CollisionFlag & CollisionSystem.CollisionFlags.Left) != CollisionSystem.CollisionFlags.None) &&
                    ((colliderFlags & CollisionSystem.CollisionFlags.Right) != CollisionSystem.CollisionFlags.None)) ||
                    (((other.CollisionFlag & CollisionSystem.CollisionFlags.Right) != CollisionSystem.CollisionFlags.None) &&
                    ((colliderFlags & CollisionSystem.CollisionFlags.Left) != CollisionSystem.CollisionFlags.None)))
                { 
                    return true;
                }
                else (Scene as LevelScene).KillPlayer();
            }
            return false;
        }

        public void Activate()
        {
            // FIX: make it so spikes that just popped up kill the player or whatever else
            Sprite.IsVisible = true;
            GetComponent<Collider>().IsActive = true;
        }

        public void Deactivate()
        {
            Sprite.IsVisible = false;
            GetComponent<Collider>().IsActive = false;
        }
    }
}
