﻿using CodeNameSmash.Objects.Player;
using CodeNameSmash.Scenes;
using Microsoft.Xna.Framework;
using PGOCS;
using PGOCS.Collisions;
using PGOCS.Components;
using PGOCS.Scenes;

namespace CodeNameSmash.Objects.Environment
{
    class Spike : EnvObject
    {
        public Spike(string id, IGame game, Scene scene, Point tilePosition) 
            : base(id, game, scene, new Sprite(game, "images/spike", new Point(16, 16), new Point(1, 1)))
        {
            Transform.Position = new Vector2(tilePosition.X * ((TiledScene)scene).Tilemap.TileWidth, tilePosition.Y * ((TiledScene)scene).Tilemap.TileHeight);
            var collider = new Collider(new Vector2(16, 16), isMovable: false);
            collider.CollisionReaction = KillPlayer;
            collider.UseDefaultAndCustomReaction = true;
            AddComponent(collider);
        }

        public bool KillPlayer(Collider other, Collision collision)
        {
            if (other.GameObject is PlayerCharacter) (Scene as LevelScene).KillPlayer();
            return false;
        }
    }
}
