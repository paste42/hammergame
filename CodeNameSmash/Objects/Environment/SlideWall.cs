﻿using Microsoft.Xna.Framework;
using PGOCS;
using PGOCS.Components;
using PGOCS.Scenes;

namespace CodeNameSmash.Objects.Environment
{
	class SlideWall : EnvObject
	{
		public Point TilePosition { get; private set; }
		private float _speed = 1;
		public enum SlideStatuses
		{
			Up,
			Down,
			MovingUp,
			MovingDown
		};
		public SlideStatuses SlideStatus { get; private set; }
        public SlideWall(string id, IGame game, Scene scene, Point tilePosition, bool initiallyUp) :
            base(id, game, scene, new Sprite(game, "images/slidewall", new Point(16, 48), new Point(1, 1)))
        {
            TilePosition = tilePosition;
            SlideStatus = initiallyUp ? SlideStatuses.Up : SlideStatuses.Down;
            Transform.Position = new Vector2(tilePosition.X * ((TiledScene)Scene).Tilemap.TileWidth,
                tilePosition.Y * ((TiledScene)Scene).Tilemap.TileHeight);
            var collider = new Collider(new Vector2(16, 48), isMovable: false)
            {
                IsMovingPlatform = true
            };
			AddComponent(collider);
		}

		public override void Update(GameTime gameTime)
		{
			float finalPosition;
			switch (SlideStatus)
			{
				case SlideStatuses.MovingDown:
					finalPosition = (TilePosition.Y + 2) * ((TiledScene) Scene).Tilemap.TileHeight;
					if (Transform.Position.Y + _speed > finalPosition)
					{
						Transform.Position = new Vector2(Transform.Position.X, finalPosition);
						SlideStatus = SlideStatuses.Down;
					} 
					else Transform.Position = new Vector2(Transform.Position.X, Transform.Position.Y + _speed);
					break;
				case SlideStatuses.MovingUp:
					finalPosition = TilePosition.Y * ((TiledScene) Scene).Tilemap.TileHeight;
					if (Transform.Position.Y - _speed < finalPosition)
					{
						Transform.Position = new Vector2(Transform.Position.X, finalPosition);
						SlideStatus = SlideStatuses.Up;
					} 
					else
						Transform.Position = new Vector2(Transform.Position.X, Transform.Position.Y - _speed);
					break;
			}
			base.Update(gameTime);
		}

		public void Toggle()
		{
			switch (SlideStatus)
			{
				case SlideStatuses.MovingDown:
					SlideStatus = SlideStatuses.MovingUp;
					break;
				case SlideStatuses.Down:
					SlideStatus = SlideStatuses.MovingUp;
					break;
				case SlideStatuses.MovingUp:
					SlideStatus = SlideStatuses.MovingDown;
					break;
				case SlideStatuses.Up:
					SlideStatus = SlideStatuses.MovingDown;
					break;
			}
		}
	}
}
