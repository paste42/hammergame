﻿using CodeNameSmash.Scenes;
using Microsoft.Xna.Framework;
using PGOCS;
using PGOCS.Components;
using PGOCS.Scenes;

namespace CodeNameSmash.Objects.Environment
{
	public class AltSwitch : EnvObject
	{
		public Type AltSwitchType { get; private set; }
		public bool IsPressed;
		public bool IsLaunching;
		public Point TilePosition { get; private set; }

		public enum Type
		{
			Type1,
			Type2
		}

		public AltSwitch(string id, IGame game, Scene scene, Type altSwitchType, Point tilePosition) : 
			base(id, game, scene, new Sprite(game, "images/altswitch", new Point(16, 32), 
				new Point(4, 1)))
		{
			TilePosition = tilePosition;
			Transform.Position = new Vector2(tilePosition.X * ((TiledScene)Scene).Tilemap.TileWidth,
				tilePosition.Y * ((TiledScene)Scene).Tilemap.TileHeight);
			AltSwitchType = altSwitchType;
			var collider = new Collider(new Vector2(16, 16), Vector2.Zero, false);
			AddComponent(collider);
			if (altSwitchType == Type.Type2) Sprite.SetFrame(2);
			IsPressed = false;
			collider.IsActive = true;
			IsLaunching = false;
		}

		public override void Update(GameTime gameTime)
		{
            // TODO: maybe. change this over to collision area
			if ((IsLaunching) && !(((LevelScene)Scene).CheckGameObjectsAgainstAltSwitch(this)))
			{
				IsLaunching = false;
				Toggle();
			}
			base.Update(gameTime);
		}

		private void ToggleCollider()
		{
			var collider = GetComponent<Collider>();
			collider.IsActive = !collider.IsActive;
		}

		public void Toggle()
		{
			IsPressed = !IsPressed;
			if (IsPressed) Sprite.SetFrame(Sprite.CurrentFrame + 1);
			else Sprite.SetFrame(Sprite.CurrentFrame - 1);
			if (!IsLaunching) ToggleCollider();
		}
	}
}
