﻿using Microsoft.Xna.Framework;
using PGOCS;
using PGOCS.Components;
using PGOCS.Scenes;

namespace CodeNameSmash.Objects.Environment
{
	class SlideSwitch : EnvObject
	{
		public Point TilePosition { get; private set; }
		private readonly SlideWall _associatedWall;
		private enum SwitchStates { Unpressed, HalfPressed, FullyPressed }

		private SwitchStates _switchState = SwitchStates.Unpressed;
		private SwitchStates _formerState = SwitchStates.Unpressed;
		private int _stateTimer;
		public SlideSwitch(string id, IGame game, Scene scene, Point tilePosition, SlideWall slideWall) :
			base(id, game, scene, new Sprite(game, "images/slideswitch", new Point(16, 20), new Point(3, 1)))
		{
			TilePosition = tilePosition;
			_associatedWall = slideWall;
			Transform.Position = new Vector2(tilePosition.X * ((TiledScene)Scene).Tilemap.TileWidth,
				tilePosition.Y * ((TiledScene)Scene).Tilemap.TileHeight);
			var collider = new Collider(new Vector2(16, 16), Vector2.Zero, false);
			AddComponent(collider);
		}

		public override void Update(GameTime gameTime)
		{
			if (_switchState == SwitchStates.FullyPressed)
				HandleTransitionState(gameTime);
			base.Update(gameTime);
		}

		private void HandleTransitionState(GameTime gameTime)
		{
			_stateTimer += gameTime.ElapsedGameTime.Milliseconds;
			if (_stateTimer < 200) return;
			switch (_formerState)
			{
				case SwitchStates.Unpressed:
					_switchState = SwitchStates.HalfPressed;
					Sprite.CurrentFrame = 2;
					break;
				case SwitchStates.HalfPressed:
					_switchState = SwitchStates.Unpressed;
					Sprite.CurrentFrame = 0;
					break;
			}
			
		}

		public void ChangeCollider()
		{
			RemoveComponent(typeof(Collider));
			switch (_formerState)
			{
				case SwitchStates.HalfPressed:
					AddComponent(new Collider(new Vector2(16, 16), Vector2.Zero, false));
					break;
				case SwitchStates.Unpressed:
					AddComponent(new Collider(new Vector2(16, 8), new Vector2(0, 8), false));
					break;
			}
		}

		public void Toggle()
		{
			_associatedWall.Toggle();
			_formerState = _switchState;
			_switchState = SwitchStates.FullyPressed;
			ChangeCollider();
			Sprite.CurrentFrame = 1;
			_stateTimer = 0;
		}
	}
}
