﻿using System.Collections.Generic;
using System.Linq;
using CodeNameSmash.Scenes;
using Microsoft.Xna.Framework;
using PGOCS;
using PGOCS.Scenes;

namespace CodeNameSmash.Objects.Environment
{
	public class AltSwitchManager : GameObject
	{
		public AltSwitchManager(string id, IGame game, Scene scene) : base(id, game, scene) { }

		public void InitSwitches(Dictionary<Point, EnvObject> envObjects, AltSwitch.Type pressedType)
		{
			foreach (var altSwitch in envObjects
				.Select(entry => entry.Value as AltSwitch)
				.Where(altSwitch => ((altSwitch != null) && (altSwitch.AltSwitchType == pressedType))))
			{
				altSwitch.Toggle();
			}
		}

		public void ActivateSwitches(Dictionary<Point, EnvObject> envObjects)
		{
			foreach (var envObject in envObjects.Select(entry => entry.Value as AltSwitch)
				.Where(envObject => (envObject != null)))
			{
				envObject.IsLaunching = ((LevelScene)Scene).CheckGameObjectsAgainstAltSwitch(envObject);
				if (!envObject.IsLaunching) envObject.Toggle();
			}
		}
	}
}
