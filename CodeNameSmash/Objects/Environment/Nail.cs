﻿using CodeNameSmash.Objects.Hammers;
using CodeNameSmash.Objects.Hammers.States;
using CodeNameSmash.Scenes;
using Microsoft.Xna.Framework;
using PGOCS;
using PGOCS.Collisions;
using PGOCS.Components;
using PGOCS.Scenes;

namespace CodeNameSmash.Objects.Environment
{
    class Nail : EnvObject
    {
        private readonly int Width = 5;
        public bool IsSinking { get; private set; }
        class TileOverlay : GameObject
        {
            public TileOverlay(IGame game, Scene scene) : base("", game, scene)
            {
                var sprite = (scene as TiledScene).GetSpriteOfTilemap();
                sprite.Transform.Position = new Vector2(0, 17);
                AddComponent(sprite);
                
            }
        }

        private TileOverlay _tileOverlay;

        public Nail(string id, IGame game, Scene scene, Point tilePosition) 
            : base(id, game, scene, new Sprite(game, "images/nail", new Point(16, 32), new Point(1, 1)))
        {
            Transform.Position = new Vector2(tilePosition.X * ((TiledScene)scene).Tilemap.TileWidth, tilePosition.Y * ((TiledScene)scene).Tilemap.TileHeight - 1);
            Transform.Depth = .5f;
            var collider = new Collider(new Vector2(5, 30), new Vector2(Width, 0), false);
            collider.CollisionReaction = ReactToCollisions;
            collider.TileCollisionReaction = DoNothing;
            AddComponent(collider);
            _tileOverlay = new TileOverlay(game, scene);
            _tileOverlay.GetComponent<Sprite>().SetFrame((scene as TiledScene).Tilemap.GetTile(tilePosition.X, tilePosition.Y + 1).Id - 1);
            AddChild(_tileOverlay);
            IsSinking = false;
        }

        public bool ReactToCollisions(Collider other, Collision collision)
        {
            if (other.GameObject is Hammer)
            {
                Hammer hammer = other.GameObject as Hammer;
                if ((hammer.CurrentHammerState is LandedState) && (GetComponent<Collider>().GetOverlapY(other) >= 2f))
                    Sink();
            }
            if (other.GameObject is AltSwitch)
            {
                if (IsSinking) (Scene as LevelScene).ToggleAltSwitches();
                // This does not get called; all popping is handled outside of collision stuff
                else PopUp();
            }
            return false;
        }

        public void DoNothing()
        { }

        public void Sink()
        {
            Transform.Position += new Vector2(0, 16);
            _tileOverlay.GetComponent<Sprite>().Transform.Position -= new Vector2(0, 16);
            GetComponent<Collider>().Position = new Vector2(Width, 1);
            IsSinking = true;
        }

        public override void Update(GameTime gameTime)
        {
            IsSinking = false;
            base.Update(gameTime);
        }

        public void PopUp()
        {
            Transform.Position -= new Vector2(0, 16);
            _tileOverlay.GetComponent<Sprite>().Transform.Position += new Vector2(0, 16);
            GetComponent<Collider>().Position = new Vector2(Width, 0);
        }
    }
}
