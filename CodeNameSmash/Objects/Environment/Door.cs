﻿using PGOCS;
using PGOCS.Scenes;
using PGOCS.Components;
using Microsoft.Xna.Framework;
using PGOCS.Collisions;
using CodeNameSmash.Objects.Player;
using CodeNameSmash.Scenes;

namespace CodeNameSmash.Objects.Environment
{
    class Door : EnvObject
    {
        private string _exit;
        private IGame _game;
        public Door(string id, IGame game, TiledScene scene, Point tilePosition, string exit) 
            : base(id, game, scene, new Sprite(game, "images/door", new Point(16, 16), new Point(1, 1)))
        {
            _game = game;
            _exit = exit;
            Transform.Position = new Vector2(tilePosition.X * scene.Tilemap.TileWidth, tilePosition.Y * scene.Tilemap.TileHeight);
            var collider = new Collider(new Vector2(2, 3), new Vector2(7, 13), false, false);
            AddComponent(collider);
            collider.IsActive = true;
            collider.StillCollidingReaction = EnterDoor;
        }

        public void EnterDoor(Collider other)
        {
            var a = Microsoft.Xna.Framework.Input.Keyboard.GetState().IsKeyDown(Microsoft.Xna.Framework.Input.Keys.Up);
            if ((other.GameObject is PlayerCharacter) && (other.GameObject as PlayerCharacter).IsEnteringDoor)
                _game.SwitchScene(new LevelScene(_game, _exit));
        }
    }
}
