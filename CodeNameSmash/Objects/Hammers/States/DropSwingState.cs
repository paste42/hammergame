﻿using Microsoft.Xna.Framework;

namespace CodeNameSmash.Objects.Hammers.States
{
    class DropSwingState : HammerState
    {
        private readonly bool _facingRight;
        private int step;
        public DropSwingState(Hammer hammer) : base(hammer)
        {
            Hammer.IsFacingRight = !Hammer.IsFacingRight;
            _facingRight = Hammer.Owner.IsFacingRight;
            step = 0;
            Hammer.SetFrame(2);
            Hammer.Transform.Position = _facingRight
                ? new Vector2(-24, Hammer.Transform.Position.Y)
                : new Vector2(-8, Hammer.Transform.Position.Y);
        }

        protected override int DurationMilliseconds
        {
            get { return 26; }
        }

        public override HammerState GetNextHammerState()
        {
            if (Timer < DurationMilliseconds)
                return this;
            Timer -= DurationMilliseconds;
            step++;
            switch (step)
            {
                case 1:
                    Hammer.SetFrame(1);
                    return this;
                case 2:
                    Hammer.SetFrame(0);
                    return this;
                case 3:
                    Hammer.SetFrame(1);
                    Hammer.IsFacingRight = !Hammer.IsFacingRight;
                    return this;
                case 4:
                    Hammer.SetFrame(2);
                    return this;
                case 5:
                    Hammer.SetFrame(8);
                    Hammer.Transform.Position = new Vector2(Hammer.Transform.Position.X, Hammer.Transform.Position.Y + 16);
                    return this;
                case 6:
                    Hammer.SetFrame(7);
                    return this;
                default:
                    break;

            }
            Hammer.IsFacingRight = _facingRight;
            Hammer.StopSwinging();
            Hammer.Drop();
            (Hammer.Scene as Scenes.LevelScene).LaunchPhysicalBody(Hammer.Owner.GetComponent<PGOCS.Components.PhysicalBody>());
            return new UnheldState(Hammer);
        }
    }
}
