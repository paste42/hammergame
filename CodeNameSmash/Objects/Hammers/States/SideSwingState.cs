﻿using CodeNameSmash.Objects.Environment;
using CodeNameSmash.Scenes;
using Microsoft.Xna.Framework;
using PGOCS.Components;
using System;

namespace CodeNameSmash.Objects.Hammers.States
{
    class SideSwingState : HammerState
    {
        private int _currentFrame;
        private readonly bool _facingRight;
        public SideSwingState(Hammer hammer) : base(hammer)
        {
            _facingRight = Hammer.Owner.IsFacingRight;
            _currentFrame = 3;
            Hammer.SetFrame(_currentFrame);
            Hammer.Transform.Position = _facingRight
                ? new Vector2(-24, Hammer.Transform.Position.Y)
                : new Vector2(-8, Hammer.Transform.Position.Y);
            Hammer.Transform.Depth = -.01f;
            Hammer.GetComponent<Collider>().IsActive = false;
        }

        protected override int DurationMilliseconds
        {
            get { return 45; }
        }

        public override HammerState GetNextHammerState()
        {
            if (Timer < DurationMilliseconds) return this;
            switch (_currentFrame)
            {
                case 3:
                    Hammer.Transform.Position = _facingRight
                        ? new Vector2(-21, Hammer.Transform.Position.Y)
                        : new Vector2(-11, Hammer.Transform.Position.Y);
                    break;
                case 4:
                    Hammer.Transform.Position = _facingRight
                        ? new Vector2(-8, Hammer.Transform.Position.Y)
                        : new Vector2(-24, Hammer.Transform.Position.Y);
                    break;
                case 5:
                    Hammer.Transform.Position = _facingRight
                        ? new Vector2(5, Hammer.Transform.Position.Y)
                        : new Vector2(-37, Hammer.Transform.Position.Y);
                    break;
                case 6:
                    if (GetBlockedStatus(Hammer.IsFacingRight, Hammer.Transform, (LevelScene)Hammer.Scene))
                    {
                        if (Timer < 200) return this;
                        return new PreSwingState(Hammer);
                    }
                    return new SideSwungState(Hammer);
                default:
                    return new PreSwingState(Hammer);
            }
            ++_currentFrame;
            Hammer.SetFrame(_currentFrame);
            Timer -= DurationMilliseconds;
            return this;
        }

        public override bool GetBlockedStatus(bool isFacingRight, Transform transform, LevelScene scene)
        {
            // TODO: Reaction to collision with Nail?
            Hammer.Transform.Position = _facingRight
                ? new Vector2(8, Hammer.Transform.Position.Y)
                : new Vector2(-40, Hammer.Transform.Position.Y);
            var collider = Hammer.GetComponent<Collider>();
            collider.Size = new Vector2(13, 13);
            collider.Position = new Vector2(isFacingRight ? 0 : 19, 19);
            var hammerPosition = Hammer.Transform.GetAbsoluteTransform().Position + collider.Position
                + new Vector2(isFacingRight ? 13 : 0, 0);
            if (scene.Tilemap.GetTile(hammerPosition) == null) return true;
            if (scene.IsSlideWallAt(collider)) return true;
            var topPoint = scene.Tilemap.GetTileCoords(hammerPosition);
            var bottomPoint = scene.Tilemap.GetTileCoords(new Vector2(hammerPosition.X, hammerPosition.Y + 13));
            Type envType = scene.GetEnvironmentTypeAt(topPoint);
            if (envType == typeof(SlideSwitch)) return true;
            envType = scene.GetEnvironmentTypeAt(bottomPoint);
            if (envType == typeof(SlideSwitch)) return true;
            var altSwitch = scene.SwitchAt(topPoint);
            if ((altSwitch != null) && (!altSwitch.IsPressed))
                return true;
            var altSwitch2 = scene.SwitchAt(bottomPoint);
            if ((altSwitch2 != null) && (!altSwitch2.IsPressed))
                return true;
            return (scene.Tilemap.GetTile(hammerPosition).Status == PGOCS.Tilemaps.Tile.TileStatus.Solid) ||
                (scene.Tilemap.GetTile(new Vector2(hammerPosition.X, (float)hammerPosition.Y + 12.999f)).Status == PGOCS.Tilemaps.Tile.TileStatus.Solid);
		}
	}
}
