﻿using CodeNameSmash.Scenes;
using Microsoft.Xna.Framework;
using PGOCS.Components;

namespace CodeNameSmash.Objects.Hammers.States
{
	abstract class HammerState : State
	{
		protected Hammer Hammer;
		protected abstract int DurationMilliseconds { get; }
		protected int Timer;

		protected HammerState(Hammer hammer)
		{
			Hammer = hammer;
		}

		public abstract HammerState GetNextHammerState();

		public override void Update(GameTime gameTime)
		{
			Timer += gameTime.ElapsedGameTime.Milliseconds;
		}

		public virtual bool GetBlockedStatus(bool isFacingRight, Transform transform, LevelScene scene)
		{
			return false;
		}

		public void SetTimer(int time)
		{
			Timer = time;
		}
	}
}
