﻿namespace CodeNameSmash.Objects.Hammers.States
{
	class PreSwingState : HammerState
	{
		protected override int DurationMilliseconds
		{
			get { return -1; }
		}

		public PreSwingState(Hammer hammer) : base(hammer)
		{
			Hammer.StopSwinging();
		}

		public override HammerState GetNextHammerState()
		{
			if (!Hammer.IsSwinging) return this;
            switch (Hammer.CurrentSwingType)
            {
                case Hammer.SwingType.Side:
                    return new SideSwingState(Hammer);
                case Hammer.SwingType.Drop:
                    return new DropSwingState(Hammer);
                case Hammer.SwingType.Poke:
                    return new PokingState(Hammer);
                default:
                    return new OverheadState(Hammer);
            }
		}
	}
}
