﻿using Microsoft.Xna.Framework;
using PGOCS.Components;
using PGOCS.Scenes;
using PGOCS.Tilemaps;

namespace CodeNameSmash.Objects.Hammers.States
{
	class SlammingState : HammerState
	{
		public bool FinishedFlag { get; set; }
		private int _durationInFrames = 0;
		protected override int DurationMilliseconds
		{
			get { return -1; }
		}

		public SlammingState(Hammer hammer) : base(hammer)
		{
			Hammer.SetFrame(2);
			Hammer.IsSlamming = true;
			var collider = Hammer.GetComponent<Collider>();
			if (collider == null) return;
			collider.Size = new Vector2(16, 16);
			collider.Position = Hammer.IsFacingRight ? new Vector2(16, 16) : new Vector2(0, 16);
			collider.IsActive = true;
			collider.TileCollisionReaction = ReactToCollision;
			collider.UseDefaultAndCustomReaction = true;
			FinishedFlag = false;
		}

		public override HammerState GetNextHammerState()
		{
			_durationInFrames++;
			Hammer.GetComponent<PhysicalBody>().Velocity = Hammer.Owner.GetComponent<PhysicalBody>().Velocity;
			if (!Hammer.GetComponent<Collider>().IsOnGround 
				&& !Hammer.Owner.GetComponent<Collider>().IsOnGround
				&& !FinishedFlag) return this;
			Hammer.IsSlamming = false;
			Hammer.GetComponent<PhysicalBody>().SetVelocityY(0f);
			Hammer.Owner.GetComponent<PhysicalBody>().SetVelocityY(0f);
			Hammer.GetComponent<Collider>().TileCollisionReaction = null;
			Hammer.GetComponent<Collider>().UseDefaultAndCustomReaction = false;
			return new LandedState(Hammer);
		}

		public void ReactToCollision()
		{
			FinishedFlag = true;
		}
	}
}
