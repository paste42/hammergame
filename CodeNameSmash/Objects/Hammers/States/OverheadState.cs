﻿using Microsoft.Xna.Framework;
using CodeNameSmash.Scenes;
using PGOCS.Components;
using PGOCS.Tilemaps;
using System;

namespace CodeNameSmash.Objects.Hammers.States
{
	class OverheadState : HammerState
	{
        int _currentFrame;
        bool _blocked;

		protected override int DurationMilliseconds
		{
			get { return 45; }
		}

        private readonly int blockedDurationMilliseconds = 180;

		public OverheadState(Hammer hammer) : base(hammer)
		{
            _currentFrame = 1;
            _blocked = false;
			Hammer.SetFrame(_currentFrame);
            Sprite sprite = Hammer.GetComponent<Sprite>();
            sprite.FlipHorizontally();
            sprite.Transform.Position = sprite.Transform.Position + new Vector2(Hammer.Owner.IsFacingRight ? -16 : 16, 0);
            Hammer.Transform.Depth = .01f;
		}

		public override HammerState GetNextHammerState()
		{
            if(_blocked)
            {
                if (Timer < blockedDurationMilliseconds)
                {
                    return this;
                }
                else return new PreSwingState(Hammer);
            }
			else if (Timer < DurationMilliseconds) return this;
            else if (_currentFrame == 1)
            {
                if (GetBlockedStatus(Hammer.Owner.IsFacingRight, Hammer.Transform.GetAbsoluteTransform(), Hammer.Scene as LevelScene))
                    _blocked = true;
                Hammer.GetComponent<Sprite>().FlipHorizontally();
                Hammer.GetComponent<Sprite>().Transform.Position = Vector2.Zero;
                _currentFrame = 0;
                Hammer.SetFrame(_currentFrame);
                Timer -= DurationMilliseconds;
                return this;
            }
			HammerState newState = new AngledState(Hammer);
			newState.SetTimer(Timer - DurationMilliseconds);
			return newState;
		}

        public override bool GetBlockedStatus(bool isFacingRight, Transform transform, LevelScene scene)
        {
            // FIX: Change Tile stuff to Collider stuff
            if (Hammer.CurrentSwingType == Hammer.SwingType.Side) return false;
            int directionAdjustment = 0;
            if (!isFacingRight) directionAdjustment = scene.Tilemap.TileWidth;
            var leftTilePosition = new Vector2(transform.Position.X + directionAdjustment, transform.Position.Y + 1);
            Point leftTileCoords = scene.Tilemap.GetTileCoords(leftTilePosition);
            if (scene.Tilemap.GetTile(leftTileCoords.X, leftTileCoords.Y).Status == Tile.TileStatus.Solid) return true;
            if (leftTilePosition.X / scene.Tilemap.TileWidth - Math.Truncate(leftTilePosition.X / scene.Tilemap.TileWidth) < .001f) return false;
            var rightTilePosition = new Vector2(leftTilePosition.X + scene.Tilemap.TileWidth, leftTilePosition.Y);
            Point rightTileCoords = scene.Tilemap.GetTileCoords(rightTilePosition);
            if (scene.Tilemap.GetTile(rightTileCoords.X, rightTileCoords.Y).Status == Tile.TileStatus.Solid) return true;
            return false;
        }
    }
}
