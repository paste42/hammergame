﻿using CodeNameSmash.Objects.Environment;
using Microsoft.Xna.Framework;
using PGOCS.Collisions;
using PGOCS.Components;
using System;

namespace CodeNameSmash.Objects.Hammers.States
{
    class PokingState : HammerState
    {
        bool _retracting;
        bool _frontCollision;
        public PokingState(Hammer hammer) : base(hammer)
        {
            Timer = 0;
            var collider = hammer.GetComponent<Collider>();
            collider.IsActive = true;
            hammer.GetComponent<Sprite>().SetFrame(2);
            hammer.Transform.Position = new Vector2(hammer.IsFacingRight ? -27 : -5, hammer.Transform.Position.Y);
            hammer.Transform.PreviousPosition = new Vector2(hammer.IsFacingRight ? -30 : -2, hammer.Transform.Position.Y);
            collider.Size = new Vector2(3, 16);
            collider.Position = new Vector2(hammer.IsFacingRight? 29 : 0, 16);
            collider.CollisionReaction = InitiateSlideOrRoll;
            _retracting = false;
            _frontCollision = false;
        }

        protected override int DurationMilliseconds
        {
            get { return 250; }
        }

        public bool InitiateSlideOrRoll(Collider other, Collision collision)
        {
            var barrel = other.GameObject as Barrel;
            if (barrel != null)
            {
                if (_frontCollision)
                    return true;
                if (barrel.CurrentState == Barrel.BarrelStates.Upright)
                    barrel.Slide(Hammer.IsFacingRight);
                else if (barrel.CurrentState == Barrel.BarrelStates.OnSide)
                    barrel.Roll(Hammer.IsFacingRight);
                _frontCollision = true;
                return false;
            }
            return true;
        }

        public void Retract(GameTime gameTime)
        {
            _retracting = true;
            int sign = Hammer.IsFacingRight ? -1 : 1;
            Hammer.Transform.Position += new Vector2(sign, 0);
            Timer = DurationMilliseconds - Timer + gameTime.ElapsedGameTime.Milliseconds;
        }

        public override HammerState GetNextHammerState()
        {
            if (Timer > DurationMilliseconds)
            {
                if (_retracting) return new PreSwingState(Hammer);
                _retracting = true;
                Timer = 0;
            }
            int sign = Hammer.IsFacingRight ? 1 : -1;
            sign *= _retracting ? -1 : 1;
            Hammer.Transform.Position += new Vector2(sign, 0);
            return this;
        }
    }
}
