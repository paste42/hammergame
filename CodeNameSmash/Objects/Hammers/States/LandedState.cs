﻿using CodeNameSmash.Objects.Environment;
using CodeNameSmash.Scenes;
using Microsoft.Xna.Framework;
using PGOCS.Collisions;
using PGOCS.Components;

namespace CodeNameSmash.Objects.Hammers.States
{
	class LandedState : HammerState
	{
		protected override int DurationMilliseconds
		{
			get { return 120; }
		}

		public LandedState(Hammer hammer) : base(hammer)
		{
			Hammer.GetComponent<Collider>().TileCollisionReaction = DoNothing;
            Hammer.GetComponent<Collider>().CollisionReaction = ReactToCollisions;
            Hammer.GetComponent<Collider>().IsActive = true;
            Hammer.Transform.PreviousPosition = Hammer.Transform.PreviousPosition - new Vector2(0, 17);
			Hammer.SetFrame(2);
		}

		public override HammerState GetNextHammerState()
		{
			if (Timer < DurationMilliseconds) return this;
			Hammer.GetComponent<Collider>().TileCollisionReaction = null;
			return new PreSwingState(Hammer);
		}

        public bool ReactToCollisions(Collider other, Collision collision)
        {
            if ((other.GameObject is AltSwitch) && (Hammer.GetComponent<Collider>().GetOverlapY(other) >= 2f)
                && (Hammer.GetComponent<Collider>().GetOverlapX(other) >= 4.5f))
            {
                (Hammer.Scene as LevelScene).ToggleAltSwitches();
                Hammer.GetComponent<Collider>().IsActive = false;
            }
            if ((other.GameObject is SlideSwitch) && (Hammer.GetComponent<Collider>().GetOverlapX(other) >= 4.5f))
                (other.GameObject as SlideSwitch).Toggle();
            return false;
        }

		public void DoNothing()
		{
		}
	}
}
