﻿using System.Collections.Generic;
using CodeNameSmash.Scenes;
using Microsoft.Xna.Framework;
using PGOCS.Collisions;
using PGOCS.Components;
using PGOCS.Tilemaps;

namespace CodeNameSmash.Objects.Hammers.States
{
	class AngledState : HammerState
	{
		protected override int DurationMilliseconds { get { return 45; }}
		private readonly int _blockedDurationMilliseconds = 180;

		public AngledState(Hammer hammer) : base(hammer)
		{
			Timer = 0;
			Hammer.SetFrame(1);
		}

		public override HammerState GetNextHammerState()
		{
			if (!Hammer.CanSwing())
			{
				if (Timer < _blockedDurationMilliseconds) return this;
				return new PreSwingState(Hammer);
			}
			if (Timer < DurationMilliseconds) return this;

            var collider = Hammer.GetComponent<Collider>();
			collider.Size = new Vector2(16, 16);
			collider.Position = Hammer.IsFacingRight ? new Vector2(16, 16) : new Vector2(0, 16);
			
			HammerState newState = new LandedState(Hammer);
			newState.SetTimer(Timer - DurationMilliseconds);
			return newState;
		}

		public override bool GetBlockedStatus(bool isFacingRight, Transform transform, LevelScene scene)
		{
			int directionAdjustment;
			if (isFacingRight) directionAdjustment = Hammer.Reach;
			else directionAdjustment = scene.Tilemap.TileWidth * 2 - Hammer.Reach;
			var topTilePosition = new Vector2(transform.Position.X + directionAdjustment,
				transform.Position.Y);
			Point topTileCoords = scene.Tilemap.GetTileCoords(topTilePosition);
            bool alignedWithTile = (transform.Position.Y % scene.Tilemap.TileHeight < .0001f);
            if (((scene.Tilemap.GetTile(topTileCoords.X, topTileCoords.Y).Status == Tile.TileStatus.Solid)
                || (scene.Tilemap.GetTile(topTileCoords.X, topTileCoords.Y + 1).Status == Tile.TileStatus.Solid)
                || ((scene.Tilemap.GetTile(topTileCoords.X, topTileCoords.Y + 2).Status == Tile.TileStatus.Solid) && !alignedWithTile)))
                return true;
            var altSwitch = scene.SwitchAt(new Point(topTileCoords.X, topTileCoords.Y + 1));
			if ((altSwitch != null) && (!altSwitch.IsPressed) && alignedWithTile)
				return false;
			var altSwitch2 = scene.SwitchAt(new Point(topTileCoords.X, topTileCoords.Y + 2));
			if ((altSwitch2 != null) && (!altSwitch2.IsPressed))
				return false;
            // FIX: Change Tile stuff to Collider stuff
            return false;
		}
	}
}
