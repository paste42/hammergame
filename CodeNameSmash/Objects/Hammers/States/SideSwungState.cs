﻿using CodeNameSmash.Objects.Environment;
using Microsoft.Xna.Framework;
using PGOCS.Collisions;
using PGOCS.Components;

namespace CodeNameSmash.Objects.Hammers.States
{
	class SideSwungState : HammerState
	{
		public SideSwungState(Hammer hammer) : base(hammer)
		{
			Hammer.Transform.Position = Hammer.IsFacingRight 
				? new Vector2(8, Hammer.Transform.Position.Y)
				: new Vector2(-40, Hammer.Transform.Position.Y);
            Hammer.Transform.PreviousPosition = Hammer.IsFacingRight
                ? new Vector2(-8, Hammer.Transform.Position.Y)
                : new Vector2(-24, Hammer.Transform.Position.Y);
            Hammer.SetFrame(3);
            var collider = Hammer.GetComponent<Collider>();
            collider.CollisionReaction = ReactToCollisions;
            collider.IsActive = true;
		}

		protected override int DurationMilliseconds
		{
			get { return 120; }
		}

        public bool ReactToCollisions(Collider other, Collision collision)
        {
            var barrel = other.GameObject as Barrel;
            if ((barrel != null) && (barrel.CurrentState == Barrel.BarrelStates.Upright))
                barrel.KnockOver();
            return false;
        }

		public override HammerState GetNextHammerState()
		{
			if (Timer < DurationMilliseconds) return this;
			return new PreSwingState(Hammer);
		}
	}
}
