﻿
namespace CodeNameSmash.Objects.Hammers.States
{
    class UnheldState : HammerState
    {
        public UnheldState(Hammer hammer) : base(hammer)
        {
        }

        protected override int DurationMilliseconds
        {
            get { return 0; }
        }

        public override HammerState GetNextHammerState()
        {
            return this;
        }
    }
}
