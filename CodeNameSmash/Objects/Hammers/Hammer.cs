﻿using CodeNameSmash.Objects.Environment;
using CodeNameSmash.Objects.Hammers.States;
using CodeNameSmash.Objects.Player;
using CodeNameSmash.Scenes;
using Microsoft.Xna.Framework;
using PGOCS;
using PGOCS.Collisions;
using PGOCS.Components;
using PGOCS.Scenes;

namespace CodeNameSmash.Objects.Hammers
{
    class Hammer : GameObject
    {
        private readonly Sprite _sprite;
		private bool _isFacingRight;
		public PlayerCharacter Owner { get; private set; }

		public HammerState CurrentHammerState { get { return _currentState;} }
	    private HammerState _currentState;
	    public SwingType CurrentSwingType;

	    public enum SwingType { Overhead, Side, Drop, Poke }
		public bool IsEnteringNewSwingStage { get; private set; }
		public bool IsBlocked { get; set; }
		public bool IsSwinging { get; protected set; }
		public bool IsAirAttacking { get; set; }
		public bool IsSlamming { get; set; }
		public bool IsStuckInWall { get; set; }
        public bool IsFacingRight 
        {
            get { return _isFacingRight; } 
            set
            {
                _isFacingRight = value;
				// TODO: change these hardcoded numbers if it varies between Hammers
                if (_isFacingRight) 
                { 
                    Transform.Position = new Vector2(-8, -32);
					GetComponent<Collider>().Position = Vector2.Zero;
                    _sprite.IsFlippedHorizontally = false;
                }
                else
                {
                    Transform.Position = new Vector2(-24, -32);
					GetComponent<Collider>().Position = new Vector2(16, 0);
                    _sprite.IsFlippedHorizontally = true;
                }
            }
        }
		public int Reach { get; protected set; }

		public Hammer(IGame game, Scene scene, PlayerCharacter owner)
			: base("hammer", game, scene)
        {
	        Reach = 28;
	        Owner = owner;
            Transform.Parent = Owner.Transform;
            _sprite = new Sprite(game, "images/temphammer", new Point(32, 34), new Point(10, 1));
            AddComponent(_sprite);
			var collider = new Collider(new Vector2(16, 16));
			AddComponent(collider);
	        collider.IsActive = false;
			collider.IsMovable = true;
            collider.CollisionReaction = ReactToCollisions;
	        //AddComponent(new PhysicalBody());
            IsFacingRight = true;
			_currentState = new PreSwingState(this);
        }

        public bool ReactToCollisions(Collider other, Collision collision)
        {
            if (other.GameObject is Nail)
                return false;
            return true;
        }

        public void Swing()
        {
			if ((CurrentSwingType == SwingType.Overhead) && !CanSwing()) return;
            _sprite.IsVisible = true;
	        _sprite.CurrentFrame = 0;
            IsSwinging = true;
            GetComponent<Collider>().IsActive = false;
	        _currentState = _currentState.GetNextHammerState();
        }

        public void Drop()
        {
            _currentState = new UnheldState(this);
            var newPosition = new Vector2(Owner.Transform.Position.X - 8, Owner.Transform.Position.Y);
            var droppedHammer = new Items.DroppedHammer(Scene.Game, Scene, newPosition);
            Scene.AddLater(droppedHammer);
            (Scene as LevelScene).AddMovableItem(droppedHammer);
        }

	    public bool CanSwing()
	    {
		    if (IsBlocked || (_currentState is UnheldState)) return false;
		    IsBlocked = _currentState.GetBlockedStatus(IsFacingRight, Transform.GetAbsoluteTransform(), (LevelScene)Scene);
		    return !IsBlocked;
	    }

        public void StopSwinging()
        {
            _sprite.IsVisible = false;
            IsSwinging = false;
	        IsAirAttacking = false;
	        IsSlamming = false;
	        IsBlocked = false;
			ResetCollider();
        }

		public void ResetCollider()
		{
			var collider = GetComponent<Collider>();
			if (collider != null)
			{
				collider.IsActive = false;
				collider.IsOnGround = false;
			}
			IsFacingRight = IsFacingRight;
		}

        public override void Update(GameTime gameTime)
        {
	        if (IsSwinging)
	        {
				_currentState.Update(gameTime);
		        _currentState = _currentState.GetNextHammerState();
	        }
            base.Update(gameTime);
        }

	    public override void LateUpdate(GameTime gameTime)
	    {
			CurrentSwingType = SwingType.Overhead;
			if (IsStuckInWall) Transform.SetAbsolutePosition(Transform.GetAbsolutePreviousPosition());
		    IsStuckInWall = false;
		    base.LateUpdate(gameTime);
	    }

	    public void SetFrame(int frameNumber)
	    {
		    _sprite.CurrentFrame = frameNumber;
	    }

        public void PickUp()
        {
            _currentState = new PreSwingState(this);
        }
    }
}
