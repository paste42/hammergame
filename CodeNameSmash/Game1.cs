﻿#region Using Statements

using System.Collections.Generic;
using System.Text;
using CodeNameSmash.Config;
using CodeNameSmash.Scenes;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using PGOCS;
using PGOCS.Config;
using PGOCS.Input;
using TiledPipelineExtLib;
using TiledSharp;

#endregion

// TODO: find out how to get possible screen resolutions and change between them

namespace CodeNameSmash
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : PGame<HammerGameConfig>
    {
	    public Dictionary<string, TmxMap> TmxMaps;
	    public Game1()
	    {
		    new GraphicsDeviceManager(this)
		    {
			    PreferredBackBufferWidth = 1280,
			    PreferredBackBufferHeight = 720
		    };

		    Content.RootDirectory = "Content";
           
			// Load DirConfig
            DirConfig = new DirConfigLoader();
			GameScale = 4f;
			IsFixedTimeStep = true;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();
			GameConfigLoader<HammerGameConfig>.Instance.LoadConfig();
            
            CurrentScene = new LevelScene(this, "basic2");
			//CurrentScene = new MenuScene(this);
            SwitchScene(CurrentScene);
            CurrentScene.ResetScene();
            //SDL.SDL_DisplayMode dm;
            //SDL.SDL_GetDisplayMode(0, 0, out dm);
            //Console.WriteLine(dm.w + "x" + dm.h);
            //graphics.ToggleFullScreen();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
	        LoadInputConfig();
			LoadTileMaps();
        }

	    private static void LoadInputConfig()
	    {
			var config = new BindingsConfigLoader().ConfigData;
			KeyMap.AssignKey(config.JumpKey, Commands.JumpCommand);
			KeyMap.AssignKey(config.OverheadSwingKey, Commands.AttackCommand);
			KeyMap.AssignKey(config.MoveRightKey, Commands.MoveRightCommand);
			KeyMap.AssignKey(config.MoveLeftKey, Commands.MoveLeftCommand);
			KeyMap.AssignKey(config.MoveDownKey, Commands.MoveDownCommand);
			KeyMap.AssignKey(config.MoveUpKey, Commands.MoveUpCommand);
			KeyMap.AssignKey(config.SideSwingModifierKey, Commands.SideSwingModifier);
            KeyMap.AssignKey(config.PokeModifierKey, Commands.PokeModifier);
            KeyMap.AssignKey(config.DropModifierKey, Commands.DropModifier);
            KeyMap.AssignKey(config.PauseKey, Commands.PauseCommand);
            KeyMap.AssignKey(config.EnterDoorKey, Commands.EnterDoorCommand);
			ButtonMap.AssignButton(new ButtonPresses.ButtonPress(config.JumpButton, PlayerIndex.One), Commands.JumpCommand);
			ButtonMap.AssignButton(new ButtonPresses.ButtonPress(config.OverheadSwingButton, PlayerIndex.One), Commands.AttackCommand);
			ButtonMap.AssignButton(new ButtonPresses.ButtonPress(config.SideSwingModifierButton, PlayerIndex.One), Commands.SideSwingModifier);
            ButtonMap.AssignButton(new ButtonPresses.ButtonPress(config.PokeModifierButton, PlayerIndex.One), Commands.PokeModifier);
            ButtonMap.AssignButton(new ButtonPresses.ButtonPress(config.DropModifierButton, PlayerIndex.One), Commands.DropModifier);
            ButtonMap.AssignButton(new ButtonPresses.ButtonPress(config.MoveRightButton, PlayerIndex.One), Commands.MoveRightCommand);
			ButtonMap.AssignButton(new ButtonPresses.ButtonPress(config.MoveLeftButton, PlayerIndex.One), Commands.MoveLeftCommand);
			ButtonMap.AssignButton(new ButtonPresses.ButtonPress(config.MoveDownButton, PlayerIndex.One), Commands.MoveDownCommand);
			ButtonMap.AssignButton(new ButtonPresses.ButtonPress(config.MoveUpButton, PlayerIndex.One), Commands.MoveUpCommand);
			ButtonMap.AssignButton(new ButtonPresses.ButtonPress(config.PauseButton, PlayerIndex.One), Commands.PauseCommand);
            ButtonMap.AssignButton(new ButtonPresses.ButtonPress(config.EnterDoorButton, PlayerIndex.One), Commands.EnterDoorCommand);
		}

		private void LoadTileMaps()
	    {
			var levelContentManager = new ContentManager(Content.ServiceProvider, "Content/");
			// TODO: maybe access map names programmatically or from config file
			TmxMaps = new Dictionary<string, TmxMap>();
            TmxMaps.Add("inescapabledeath", new TmxMap(Encoding.Default.GetString(levelContentManager.Load<TmxXml>("maps/inescapabledeath").BinaryMap), true));
            TmxMaps.Add("bouncezone", new TmxMap(Encoding.Default.GetString(levelContentManager.Load<TmxXml>("maps/bouncezone").BinaryMap), true));
            TmxMaps.Add("jumptesting", new TmxMap(Encoding.Default.GetString(levelContentManager.Load<TmxXml>("maps/jumptesting").BinaryMap), true));
            TmxMaps.Add("dropandreset", new TmxMap(Encoding.Default.GetString(levelContentManager.Load<TmxXml>("maps/dropandreset").BinaryMap), true));
            TmxMaps.Add("basic3", new TmxMap(Encoding.Default.GetString(levelContentManager.Load<TmxXml>("maps/basic3").BinaryMap), true));
			TmxMaps.Add("basic2", new TmxMap(Encoding.Default.GetString(levelContentManager.Load<TmxXml>("maps/basic2").BinaryMap), true));
			TmxMaps.Add("basic", new TmxMap(Encoding.Default.GetString(levelContentManager.Load<TmxXml>("maps/basic").BinaryMap), true));
	    }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Check exit buttons
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
            if (IsActive) base.Update(gameTime);
        }
    }
}
