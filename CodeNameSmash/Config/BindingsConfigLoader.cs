﻿using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace CodeNameSmash.Config
{
	class BindingsConfigLoader
	{
		private readonly BindingsConfig _configData;
        public BindingsConfig ConfigData { get { return _configData; } }

		public BindingsConfigLoader(string filePath = "./Config/bindingsconfig.json")
        {
			JsonConverter converter = new StringEnumConverter();
            _configData = JsonConvert.DeserializeObject<BindingsConfig>(File.ReadAllText(filePath), converter);
        }
	}
}
