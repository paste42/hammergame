﻿using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace CodeNameSmash.Config
{
	class LevelConfigLoader
	{
		private LevelConfig _config;
		public LevelConfig ConfigData {get {return _config;}}
		public LevelConfigLoader(string level)
		{
			JsonConverter converter = new StringEnumConverter();
			_config = JsonConvert.DeserializeObject<LevelConfig>(File.ReadAllText("./Levels/" + level + ".json"), converter);
		}
	}
}
