﻿using Microsoft.Xna.Framework.Input;

namespace CodeNameSmash.Config
{
	public class BindingsConfig
	{
		public Keys JumpKey;
		public Keys OverheadSwingKey;
		public Keys MoveRightKey;
		public Keys MoveLeftKey;
		public Keys MoveUpKey;
		public Keys MoveDownKey;
		public Keys SideSwingModifierKey;
        public Keys PokeModifierKey;
        public Keys DropModifierKey;
		public Keys PauseKey;
        public Keys EnterDoorKey;
		public Buttons JumpButton;
		public Buttons OverheadSwingButton;
		public Buttons MoveRightButton;
		public Buttons MoveLeftButton;
		public Buttons MoveUpButton;
		public Buttons MoveDownButton;
		public Buttons SideSwingModifierButton;
        public Buttons PokeModifierButton;
        public Buttons DropModifierButton;
		public Buttons PauseButton;
        public Buttons EnterDoorButton;
	}
}
