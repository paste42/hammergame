﻿using PGOCS.Config;

namespace CodeNameSmash.Config
{
    public class HammerGameConfig : GameConfig
    {
        public float PlayerAccelerationMagnitude;
        public float PlayerMaxSpeed;
        public float PlayerInitalJumpSpeed;
        public float PlayerSpringBootsJumpSpeed;
        public float PlayerSecondaryJumpSpeed;
        public float SceneGravity;
		public float GoombaMaxSpeed;
    }
}
