﻿using PGOCS.Config;

namespace CodeNameSmash.Config
{
    public class HammerGameConfigManager : GameConfigManager<HammerGameConfig>
    {
        //private static HammerGameConfig config;
        private static HammerGameConfigManager _instance;
        private HammerGameConfigManager() { }

        public static HammerGameConfigManager Instance
        {
            get 
            {
                if (_instance == null) _instance = new HammerGameConfigManager();
                return _instance;
            }
        }

        public float PlayerAccelerationMagnitude
        { get { return UseConfig(c => c.PlayerAccelerationMagnitude); } }

        public float PlayerMaxSpeed
        { get { return UseConfig(c => c.PlayerMaxSpeed); } }

        public float PlayerSecondaryJumpSpeed
        { get { return UseConfig(c => c.PlayerSecondaryJumpSpeed); } }

        public float PlayerInitialJumpSpeed
        { get { return UseConfig(c => c.PlayerInitalJumpSpeed); } }

        public float PlayerSpringBootsJumpSpeed
        { get { return UseConfig(c => c.PlayerSpringBootsJumpSpeed); } }

		public float SceneGravity
		{ get { return UseConfig(c => c.SceneGravity); } }
	
		public float GoombaMaxSpeed
		{ get { return UseConfig(c => c.GoombaMaxSpeed); } }
	}
}
