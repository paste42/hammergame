﻿using CodeNameSmash.Objects.Environment;

namespace CodeNameSmash.Config
{
	class LevelConfig
	{
#pragma warning disable 0649
        public class AltStructData
		{
			public string Id;
			public int TilePosX;
			public int TilePosY;
			public AltSwitch.Type SwitchType;
		}

		public class SlideWallAndSwitchData
		{
			public string Id;
			public int TilePosX;
			public int TilePosY;
			public bool InitiallyUp;
			public int SwitchTilePosX;
			public int SwitchTilePosY;
		}
		public float PlayerInitialPositionX;
		public float PlayerInitialPositionY;
		public AltStructData[] AltSwitches;
		public AltSwitch.Type InitialPressedSwitch = AltSwitch.Type.Type1;
		public SlideWallAndSwitchData[] SlideWallsAndSwitches;

        public class NailData
        {
            public string Id;
            public int TilePosX;
            public int TilePosY;
            public bool InitiallyUp;
        }
        public NailData[] Nails;

        public class SpikeData
        {
            public string Id;
            public int TilePosX;
            public int TilePosY;
        }
        public SpikeData[] Spikes;

		public class GoombaData
		{
			public string Id;
			public int PosX;
			public int PosY;
		}
		public GoombaData[] Goombas;

        public class WalkingHatData
        {
            public string Id;
            public int PosX;
            public int PosY;
        }
        public WalkingHatData[] WalkingHats;

        public class PressurePlateData
        {
            public string Id;
            public int TilePosX;
            public int TilePosY;
            public string[] AssociatedSpikes;
        }
        public PressurePlateData[] PressurePlates;

        public class PressureSpikeData
        {
            public string Id;
            public int TilePosX;
            public int TilePosY;
        }
        public PressureSpikeData[] PressureSpikes;

		public class GoalItemData
		{
			public string Id;
			public int TilePosX;
			public int TilePosY;
			public string Texture;
		}
        public GoalItemData[] GoalItems;

        public class DoorData
        {
            public string Id;
            public int TilePosX;
            public int TilePosY;
            public string Exit;
        }
        public DoorData[] Doors;

        public class BarrelData
        {
            public string Id;
            public int TilePosX;
            public int TilePosY;
            public bool IsUpright;
        }
        public BarrelData[] Barrels;
#pragma warning restore 0649
    }
}
