﻿using CodeNameSmash.Objects.Menu;
using Microsoft.Xna.Framework;
using PGOCS;
using PGOCS.Input;
using PGOCS.Scenes;

namespace CodeNameSmash.Scenes
{
	class MenuScene : Scene
	{
		private Cursor _cursor;
		public MenuScene(IGame game) : base(game, 1f)
		{
			
		}

		protected override void LoadContent()
		{
			_cursor = new Cursor("cursor", Game, this);
			Add(_cursor);
			base.LoadContent();
		}

		public override void Update(GameTime gameTime)
		{
			if (InputManager.WasInputPressed(Commands.MoveLeftCommand)) _cursor.MoveLeft();
			if (InputManager.WasInputPressed(Commands.MoveRightCommand)) _cursor.MoveRight();
			if (InputManager.WasInputPressed(Commands.MoveUpCommand)) _cursor.MoveUp();
			if (InputManager.WasInputPressed(Commands.MoveDownCommand)) _cursor.MoveDown();
			base.Update(gameTime);
		}
	}
}
