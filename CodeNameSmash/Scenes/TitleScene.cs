﻿using System;
using Microsoft.Xna.Framework;
using PGOCS;
using PGOCS.Scenes;

namespace CodeNameSmash.Scenes
{
    public class TitleScene : Scene
    {
        public TitleScene(IGame game) : base(game, Game1.GameScale)
        {

        }

        protected override void LoadContent()
        {
        }

        public override void Update(GameTime gameTime)
        {
            // TODO: implement title screen

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            Game.GraphicsDevice.Clear(Color.Crimson);
            base.Draw(gameTime);
        }

        public override void ResetScene()
        {
            throw new NotImplementedException();
        }

    }
}
