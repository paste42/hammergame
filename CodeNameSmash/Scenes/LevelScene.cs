﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using CodeNameSmash.Config;
using CodeNameSmash.Objects.Enemies;
using CodeNameSmash.Objects.Environment;
using CodeNameSmash.Objects.Hammers.States;
using CodeNameSmash.Objects.Interfaces;
using CodeNameSmash.Objects.Items;
using CodeNameSmash.Objects.Overlay;
using CodeNameSmash.Objects.Player;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using PGOCS;
using PGOCS.Collisions;
using PGOCS.Components;
using PGOCS.Input;
using PGOCS.Scenes;
using PGOCS.Tilemaps;

namespace CodeNameSmash.Scenes
{
    public class LevelScene : TiledScene, IPausable
    {
        // TODO: Make overhead attacks destroy barrels
        // TODO: Make spikes destroy barrels (and other things)
        // TODO: Make a barrel landing on the player or rolling over the player kill the player
        // TODO: add barrel as moving platform?
        // TODO: add death screen transitions
        // TODO: make it so player and enemies get squished if between toggling altswitch and ceiling
        // TODO: implement door interactivity
        // TODO: implement hammer GET! 
        PlayerCharacter _player;
	    private Camera _levelAreaCamera;
	    private readonly string _mapFilename;

        #region debugvars
        private bool _isFrozen;
        private bool _showTileNumbers;
        private bool _slowMotionToggle;
        public string _debugInfo { get; private set; }
        #endregion

        List<Enemy> _enemies;
        List<GameObject> _movingItems;
	    private Dictionary<Point, EnvObject> _envObjects;
	    private Dictionary<Point, GoalItem> _goalItems;
        private Dictionary<Point, Door> _doors;
        private List<GameObject> _removalList;
	    private AltSwitchManager _altSwitchManager;
	    private SpriteFont font;

	    public LevelScene(IGame game, string mapFilename)
		    : base(game, "images/simpletiles", mapFilename, Game1.GameScale)
	    {
		    _levelAreaCamera = new Camera("levelareacamera", game, this, new Vector2(320, 160));
		    _mapFilename = mapFilename;
		    IsPaused = false;
	    }

        protected override void LoadContent()
        {
	        _isFrozen = false;
            _showTileNumbers = false;
			base.LoadContent(); 
			
			//TODO: add checksum check to verify files?
			var config = new LevelConfigLoader(_mapFilename).ConfigData;
			_player = new PlayerCharacter("player", Game, this, "images/mcsmaller", new Vector2(config.PlayerInitialPositionX, config.PlayerInitialPositionY));
            Add(_player);
            _levelAreaCamera.FocusedObject = _player;
			_levelAreaCamera.FocusOffset = new Vector2(0, 1);

			Overlay.Add(new OverlayBar(Game, this, "images/testoverlay"));

            _movingItems = new List<GameObject>();
            _removalList = new List<GameObject>();
            SpawnEnvironmentObjects(config);
			SpawnEnemies(config);
	        SpawnGoalItems(config);
            SpawnDoors(config);
            
	        font = Game.Content.Load<SpriteFont>("fonts/DebugFont");
        }

	    private void SpawnEnvironmentObjects(LevelConfig config)
	    {
			_envObjects = new Dictionary<Point, EnvObject>();
            if (config.AltSwitches != null)
            {
                foreach (LevelConfig.AltStructData data in config.AltSwitches)
                {
                    Point pos = new Point(data.TilePosX, data.TilePosY);
                    _envObjects.Add(pos, new AltSwitch(data.Id, Game, this, data.SwitchType, pos));
                    Add(_envObjects[pos]);
                }
                _altSwitchManager = new AltSwitchManager("altswitchmanager", Game, this);
                _altSwitchManager.InitSwitches(_envObjects, config.InitialPressedSwitch);
            }

            if (config.SlideWallsAndSwitches != null)
            {
                foreach (LevelConfig.SlideWallAndSwitchData data in config.SlideWallsAndSwitches)
                {
                    Point pos = new Point(data.TilePosX, data.TilePosY);
                    _envObjects.Add(pos, new SlideWall(data.Id, Game, this, pos, data.InitiallyUp));
                    Add(_envObjects[pos]);
                    Point switchPos = new Point(data.SwitchTilePosX, data.SwitchTilePosY);
                    _envObjects.Add(switchPos,
                        new SlideSwitch(data.Id + "switch", Game, this, switchPos, _envObjects[pos] as SlideWall));
                    Add(_envObjects[switchPos]);
                }
            }

            if (config.Nails != null)
                foreach (LevelConfig.NailData data in config.Nails)
                {
                    Point pos = new Point(data.TilePosX, data.TilePosY);
                    _envObjects.Add(pos, new Nail(data.Id, Game, this, pos));
                    Add(_envObjects[pos]);
                }

            if (config.Spikes != null)
                foreach (LevelConfig.SpikeData data in config.Spikes)
                {
                    Point pos = new Point(data.TilePosX, data.TilePosY);
                    _envObjects.Add(pos, new Spike(data.Id, Game, this, pos));
                    Add(_envObjects[pos]);
                }

            if (config.PressureSpikes != null)
                foreach (LevelConfig.PressureSpikeData data in config.PressureSpikes)
                {
                    Point pos = new Point(data.TilePosX, data.TilePosY);
                    _envObjects.Add(pos, new PressureSpike(data.Id, Game, this, pos));
                    Add(_envObjects[pos]);
                }

            if (config.PressurePlates != null)
                foreach (LevelConfig.PressurePlateData data in config.PressurePlates)
                {
                    Point pos = new Point(data.TilePosX, data.TilePosY);
                    _envObjects.Add(pos, new PressurePlate(data.Id, Game, this, pos));
                    foreach (EnvObject spike in _envObjects.Values.Where(x => x is PressureSpike))
                    {
                        if (data.AssociatedSpikes.Contains(spike.Name))
                            ((PressurePlate)_envObjects[pos]).AddAssociatedEnvObject(spike);
                    }
                    Add(_envObjects[pos]);
                }

            if (config.Barrels != null)
                foreach (LevelConfig.BarrelData data in config.Barrels)
                {
                    Point pos = new Point(data.TilePosX, data.TilePosY);
                    var barrel = new Barrel(data.Id, Game, this, pos, data.IsUpright);
                    Add(barrel);
                    AddMovableItem(barrel);
                }
	    }

		private void SpawnEnemies(LevelConfig config)
	    {
			_enemies = new List<Enemy>();
			if (config.Goombas != null) 
			    foreach (LevelConfig.GoombaData data in config.Goombas)
				    Goomba.Spawn(data.Id, Game, this, new Vector2(data.PosX, data.PosY));
            if (config.WalkingHats != null)
                foreach (LevelConfig.WalkingHatData data in config.WalkingHats)
                    WalkingHat.Spawn(data.Id, Game, this, new Vector2(data.PosX, data.PosY));
	    }

	    private void SpawnGoalItems(LevelConfig config)
	    {
		    _goalItems = new Dictionary<Point, GoalItem>();
		    if (config.GoalItems == null) return;
		    foreach (var data in config.GoalItems)
		    {
			    var item = new GoalItem(data.Id, Game, this, new Point(data.TilePosX, data.TilePosY), "images/" + data.Texture);
				_goalItems.Add(new Point(data.TilePosX, data.TilePosY), item);
			    Add(item);
		    }
	    }

        private void SpawnDoors(LevelConfig config)
        {
            _doors = new Dictionary<Point, Door>();
            if (config.Doors == null) return;
            foreach (var data in config.Doors)
            {
                var door = new Door(data.Id, Game, this, new Point(data.TilePosX, data.TilePosY), data.Exit);
                _doors.Add(new Point(data.TilePosX, data.TilePosY), door);
                Add(door);
            }
        }

        protected override void LoadTilemap()
        {
			Tilemap = new Tilemap(((Game1)Game).TmxMaps[_mapFilename]);
            for (int i = 0; i < Tilemap.Height; ++i)
                for (int j = 0; j < Tilemap.Width; ++j)
                    if (Tilemap.GetTile(j, i).Id > 1)
                        Tilemap.SetTileState(j, i, Tile.TileStatus.Solid);
        }

	    public override void Update(GameTime gameTime)
	    {
		    bool continueUpdate = true;
			HandleDebugInput(ref continueUpdate);
		    if (!continueUpdate) return;
			ProcessInput();
			if (IsPaused) return;
			base.Update(gameTime);
            PrepareCollisions();
            Collide();
            RemoveObjects();
		}

        private void PrepareCollisions()
        {
            foreach (EnvObject eo in _envObjects.Values)
            {
                CollisionSystem.Collide(_player, eo);
                foreach (GameObject go in _movingItems)
                    if (go != eo) CollisionSystem.Collide(go, eo);
                foreach (Enemy e in _enemies)
                    CollisionSystem.Collide(e, eo);
                if ((eo is Nail) && ((eo as Nail).IsSinking))
                {
                    CollisionSystem.Collide(eo, _player);
                    foreach (EnvObject eo2 in _envObjects.Values.Where(x => x != eo))
                        CollisionSystem.Collide(eo, eo2);
                    foreach (GameObject go in _movingItems)
                        CollisionSystem.Collide(eo, go);
                }
            }
            foreach (Door d in _doors.Values)
                CollisionSystem.Collide(_player, d);
            foreach (GoalItem gi in _goalItems.Values)
                CollisionSystem.Collide(_player, gi);
            foreach (GameObject go in _movingItems)
            {
                CollisionSystem.Collide(_player, go);
                foreach (GameObject mi in _movingItems.Where(x => x != go))
                    CollisionSystem.Collide(go, mi);
                foreach (EnvObject eo in _envObjects.Values)
                    CollisionSystem.Collide(go, eo);
                foreach (Enemy e in _enemies)
                {
                    CollisionSystem.Collide(e, go);
                    CollisionSystem.Collide(go, e);
                }
            }
            foreach (Enemy e in _enemies)
            {
                CollisionSystem.Collide(_player, e);
                foreach (Enemy e2 in _enemies.Where(x => !x.Equals(e)))
                    CollisionSystem.Collide(e, e2);
            }
            if (_player.Hammer.GetComponent<Collider>().IsActive)
            {
                foreach (EnvObject eo in _envObjects.Values) CollisionSystem.Collide(_player.Hammer, eo);
                foreach (GameObject go in _movingItems) CollisionSystem.Collide(_player.Hammer, go);
                foreach (Enemy e in _enemies) CollisionSystem.Collide(_player.Hammer, e);
            }

        }

        private void Collide()
        {
            CollisionSystem.CollideWithTilemap(_player, Tilemap);
            CollisionSystem.CollideWithTilemap(_enemies, Tilemap);
            CollisionSystem.CollideWithTilemap(_movingItems, Tilemap);
            // TODO: make hammer pokes affect other objects
            if (_player.Hammer.GetComponent<Collider>().IsActive)
                CollisionSystem.CollideWithTilemap(_player.Hammer, Tilemap);
            foreach (EnvObject eo in _envObjects.Values.Where(x => (x is Nail) && ((x as Nail).IsSinking)))
            {
                CollisionSystem.Collide(eo, _player);
                foreach (EnvObject eo2 in _envObjects.Values.Where(x => x != eo))
                    CollisionSystem.Collide(eo, eo2);
                foreach (GameObject go in _movingItems)
                    CollisionSystem.Collide(eo, go);
                foreach (Enemy e in _enemies)
                    CollisionSystem.Collide(eo, e);
                CollisionSystem.CollideWithTilemap(eo, Tilemap);
            }
            CollisionSystem.CollideWithTilemap(_player, Tilemap);
            CollisionSystem.CollideWithTilemap(_enemies, Tilemap);
            foreach (var go in _movingItems.Where(x => x is Barrel).Select(x => x as Barrel))
                if (go.CurrentState == Barrel.BarrelStates.Sliding)
                    foreach (var eo in _envObjects.Values.Where(x => x != go))
                        CollisionSystem.Collide(go, eo);
            CollisionSystem.CollideWithTilemap(_player, Tilemap);
            CollisionSystem.CollideWithTilemap(_movingItems, Tilemap);
            if (_player.Hammer.GetComponent<Collider>().IsActive)
                CollisionSystem.CollideWithTilemap(_player.Hammer, Tilemap);
            foreach (GameObject go in _movingItems)
            {
                if ((_player.GetComponent<Collider>().CheckSimpleCollision(go.GetComponent<Collider>()))
                    && (go.GetComponent<Collider>().CollisionReaction != null))
                    go.GetComponent<Collider>().CollisionReaction.Invoke(_player.GetComponent<Collider>(), 
                        new Collision(_player.GetComponent<Collider>(), go.GetComponent<Collider>()));
            }
            CollisionSystem.CollideWithTilemap(_player, Tilemap);
            CollisionSystem.CollideWithTilemap(_movingItems, Tilemap);
            if (_player.Hammer.GetComponent<Collider>().IsActive)
                CollisionSystem.CollideWithTilemap(_player.Hammer, Tilemap);
            foreach (GameObject go in _movingItems)
            {
                if ((_player.GetComponent<Collider>().CheckSimpleCollision(go.GetComponent<Collider>()))
                    && (go.GetComponent<Collider>().CollisionReaction != null))
                    go.GetComponent<Collider>().CollisionReaction.Invoke(_player.GetComponent<Collider>(),
                        new Collision(_player.GetComponent<Collider>(), go.GetComponent<Collider>()));
            }
            CollisionSystem.CollideWithTilemap(_player, Tilemap);
            CollisionSystem.CollideWithTilemap(_enemies, Tilemap);
            _debugInfo = string.Format("player velocity Y: {0}", _player.GetComponent<PhysicalBody>().Velocity.Y);
        }

        [Conditional("DEBUG")]
	    private void HandleDebugInput(ref bool continueUpdate)
	    {
            _slowMotionToggle = !_slowMotionToggle;
			if (KeyPresses.WasKeyHeld(Keys.RightShift) && _slowMotionToggle) continueUpdate = false;
			if (KeyPresses.WasKeyPressed(Keys.F)) _isFrozen = !_isFrozen;
            if (KeyPresses.WasKeyPressed(Keys.G)) _showTileNumbers = !_showTileNumbers;
			if (_isFrozen && !KeyPresses.WasKeyPressed(Keys.Tab))
			{
				foreach (Keys key in Keyboard.GetState().GetPressedKeys()
					.Where(key => (key != Keys.F) && (key != Keys.Tab)))
					KeyPresses.PressKeyUntilChecked(key);
				foreach (PlayerIndex player in Enum.GetValues(typeof(PlayerIndex)))
				{
					if (!GamePad.GetState(player).IsConnected) continue;
					foreach (Buttons button in Enum.GetValues(typeof(Buttons)).Cast<Buttons>().Where(button => GamePad.GetState(player).IsButtonDown(button)))
						ButtonPresses.PressButtonUntilChecked(new ButtonPresses.ButtonPress(button, player));
				}
				continueUpdate = false;
			}
			if (KeyPresses.WasKeyPressed(Keys.Back)) ToggleAltSwitches();
            if (KeyPresses.WasKeyPressed(Keys.R)) ResetScene();
	    }

	    public void ProcessInput()
	    {
			if (InputManager.WasInputPressed(Commands.PauseCommand)) Commands.PauseCommand(_player);
			if (IsPaused) return;
		    if (InputManager.IsInputPressed(Commands.MoveLeftCommand)) Commands.MoveLeftCommand(_player);
			if (InputManager.IsInputPressed(Commands.MoveRightCommand)) Commands.MoveRightCommand(_player);
			if (InputManager.IsInputPressed(Commands.SideSwingModifier)) Commands.SideSwingModifier(_player);
            if (InputManager.IsInputPressed(Commands.PokeModifier)) Commands.PokeModifier(_player);
            if (InputManager.IsInputPressed(Commands.DropModifier)) Commands.DropModifier(_player);
			if (InputManager.WasInputPressed(Commands.AttackCommand)) Commands.AttackCommand(_player);
			if (InputManager.IsInputPressed(Commands.JumpCommand)) Commands.JumpCommand(_player);
            if (InputManager.IsInputPressed(Commands.EnterDoorCommand)) Commands.EnterDoorCommand(_player);
	    }

	    public override void LateUpdate(GameTime gameTime)
	    {
			if ((_isFrozen) && (!KeyPresses.WasKeyPressed(Keys.Tab))) return;
		    base.LateUpdate(gameTime);
			_levelAreaCamera.Update(gameTime, Size);
	    }

	    public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime, _levelAreaCamera);
            SpriteBatch.Begin();
			if (_debugInfo != null) SpriteBatch.DrawString(font, _debugInfo, Vector2.Zero, Color.White);
            if (_showTileNumbers) DrawTileNumbers(gameTime);
		    SpriteBatch.End();
        }

        [Conditional("DEBUG")]
        private void DrawTileNumbers(GameTime gameTime)
        {
            Vector2 adjustedPosition = _levelAreaCamera.GetScreenPosition(Vector2.Zero);
            for (int i = 0; i < Tilemap.Width; ++i)
                for (int j = 0; j < Tilemap.Height; ++j)
                    SpriteBatch.DrawString(font, i + "," + j, new Vector2(i * Tilemap.TileWidth, j * Tilemap.TileHeight) + adjustedPosition + Vector2.One, Color.White);
        }

	    public Type GetEnvironmentTypeAt(Point tile)
	    {
		    return _envObjects.ContainsKey(tile) ? _envObjects[tile].GetType() : null;
	    }

        public void ToggleAltSwitches()
        {
            _altSwitchManager.ActivateSwitches(_envObjects);
        }

        public bool IsSlideWallAt(Collider other)
        {
            foreach (EnvObject eo in _envObjects.Values)
                if (eo is SlideWall)
                {
                    if ((eo as SlideWall).GetComponent<Collider>().CheckSimpleCollision(other)) return true;
                }
            return false;
        }

	    public AltSwitch SwitchAt(Point coords)
	    {
		    if (!_envObjects.ContainsKey(coords)) return null;
		    return _envObjects[coords] as AltSwitch;
	    }

	    public void LaunchPhysicalBody(PhysicalBody physicalBody)
	    {
			physicalBody.SetVelocityY(-300);
	    }

	    public bool CheckGameObjectsAgainstAltSwitch(AltSwitch altSwitch)
	    {
		    bool playerLaunched = CheckPlayerAgainstAltSwitch(altSwitch);
		    bool enemyLaunched = CheckEnemiesAgainstAltSwitch(altSwitch);
            bool movingObjectLaunched = CheckMovingObjectsAgainstAltSwitch(altSwitch);
            CheckNailAgainstAltSwitch(altSwitch);
		    return  playerLaunched || enemyLaunched || movingObjectLaunched;
	    }

        public bool CheckMovingObjectsAgainstAltSwitch(AltSwitch altSwitch)
        {
            bool result = false;
            foreach (var movingObject in _movingItems)
                if ((altSwitch.IsPressed) && (movingObject.GetComponent<Collider>().CheckSimpleCollision(altSwitch.GetComponent<Collider>())))
                {
                    result = true;
                    if (movingObject is ILaunchable) (movingObject as ILaunchable).Launch();
                    LaunchPhysicalBody(movingObject.GetComponent<PhysicalBody>());
                }
            return result;
        }

        public void CheckNailAgainstAltSwitch(AltSwitch altSwitch)
        {
            foreach (var nail in _envObjects.Select(o => o.Value as Nail).Where(nail => nail != null))
            {
                if ((altSwitch.IsPressed) && (nail.GetComponent<Collider>().CheckSimpleCollision(altSwitch.GetComponent<Collider>())))
                    nail.PopUp();
            }
        }

	    public bool CheckPlayerAgainstAltSwitch(AltSwitch altSwitch)
	    {
		    bool result = _player.GetComponent<Collider>().CheckSimpleCollision(altSwitch.GetComponent<Collider>());
			if (result) LaunchPhysicalBody(_player.GetComponent<PhysicalBody>());
		    return result;
	    }

	    public bool CheckEnemiesAgainstAltSwitch(AltSwitch altSwitch)
	    {
		    bool finalResult = false;
		    foreach (Enemy e in _enemies)
		    {
			    var result = e.GetComponent<Collider>().CheckSimpleCollision(altSwitch.GetComponent<Collider>());
			    if (!result) continue;
			    LaunchPhysicalBody(e.GetComponent<PhysicalBody>());
			    finalResult = true;
		    }
		    return finalResult;
	    }

        public void KillPlayer()
        {
            ResetScene();
        }

	    public void AddEnemy(Enemy enemy)
	    {
		    _enemies.Add(enemy);
	    }

        public void AddMovableItem(GameObject go)
        {
            _movingItems.Add(go);
        }

        public void MarkGameObjectForRemoval(GameObject go)
        {
            _removalList.Add(go);
        }

        public void RemoveObjects()
        {
            foreach (GameObject go in _removalList)
                RemoveObject(go);
            _removalList.Clear();
        }

	    public void RemoveObject(GameObject gameObject)
	    {
            if (gameObject is Enemy e) _enemies.Remove(e);
            if (gameObject is EnvObject eo) _envObjects.Remove(_envObjects.First(x => x.Value == eo).Key);
            if (_movingItems.Contains(gameObject)) _movingItems.Remove(gameObject);
            Remove(gameObject);
	    }

	    public bool IsPaused { get; set; }

	    void IPausable.Pause()
	    {
			IsPaused = !IsPaused;
			foreach (var go in GOList.OfType<IPausable>())
			    (go).Pause();
	    }

        internal PlayerCharacter GetPlayer()
        {
            return _player;
        }
    }
}
