﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PGOCS;
using PGOCS.Scenes;

namespace CodeNameSmash.Scenes
{
    public class SplashScene : Scene
    {
        Texture2D _splashLogo;
        public SplashScene(IGame game) : base(game, Game1.GameScale)
        {
            
        }

        protected override void LoadContent()
        {
            //_splashLogo = AssetManager.GetTexture(@"images/splashLogo");
        }

        public override void Update(GameTime gameTime)
        {
            // TODO: implement logo
            if (SceneTimer >= 1000)
                Game.SwitchScene(new LevelScene(Game, "maps/basic2"));
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            Game.GraphicsDevice.Clear(Color.Bisque);
            base.Draw(gameTime);
        }
    }
}
