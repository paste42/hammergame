﻿using CodeNameSmash.Objects.Interfaces;
using CodeNameSmash.Objects.Player;
using PGOCS;
using PGOCS.Scenes;

namespace CodeNameSmash
{
	static class Commands
	{
		public static void JumpCommand(GameObject gameObject)
		{
			((IJumper)gameObject).Jump();
		}

		public static void MoveRightCommand(GameObject gameObject)
		{
			((IHorizontalMover)gameObject).MoveRight();
		}

		public static void MoveLeftCommand(GameObject gameObject)
		{
			((IHorizontalMover)gameObject).MoveLeft();
		}

        public static void EnterDoorCommand(GameObject gameObject)
        {
            ((PlayerCharacter)gameObject).EnterDoor();
        }

		public static void MoveUpCommand(GameObject gameObject)
		{
			((IVerticalMover)gameObject).MoveUp();
		}
		public static void MoveDownCommand(GameObject gameObject)
		{
			((IVerticalMover)gameObject).MoveDown();
		}

		public static void AttackCommand(GameObject gameObject)
		{
			((IAttacker)gameObject).Attack();
		}

		public static void SideSwingModifier(GameObject gameObject)
		{
			((IAttacker)gameObject).SetSideSwingModifier();
		}

        public static void PokeModifier(GameObject gameObject)
        {
            ((IAttacker)gameObject).SetPokeModifier();
        }

        public static void DropModifier(GameObject gameObject)
        {
            ((IAttacker)gameObject).SetDropModifier();
        }

		public static void PauseCommand(GameObject dummy)
		{
			((IPausable)dummy.Scene).Pause();
		}
	}
}
