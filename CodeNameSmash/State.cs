﻿using Microsoft.Xna.Framework;

namespace CodeNameSmash
{
	abstract class State
	{
		public virtual void Update(GameTime gameTime) { }
		public virtual void Enter() { }
		public virtual void Exit() { }
	}
}
